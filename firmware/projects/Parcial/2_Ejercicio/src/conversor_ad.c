/*! @mainpage conversor_ad
 *
 * \section genDesc General Description
 *
 *
 *
 *Disparo de conversión AD con interrupción periódica de timer de 1/15Hz (67ms).
 *
 *Obtención de datos analógicos mediante el CH3 y se los convierte a digitales, es controlada por el timer.
 *Cada 1s se muestra el promedio de las muestras acumuladas en ese tiempo.
 *

 * \section hardConn Hardware Connection
 *
 * | Potenciometro  |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	OUT 	    | 	CH3         |
 * |    GND         |   GNDA        |
 * |    VCC         |   VDDA         |
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 02/11/2020 | Creacion del documento		                 |
 *
 * @author Roda Rocío Milagros
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/conversor_ad.h"       /* <= own header */
#include "systemclock.h"
#include "bool.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"
#include "led.h"


/*==================[macros and definitions]=================================*/

#define COUNT_DELAY 3000000
#define FALSE 0
#define TRUE 1
#define BAUDIO 115200 /*Velocidad de transmisión por UART*/

bool adc=FALSE;
/*Bandera para conversión AC */
uint16_t valor; /*Creo una variable que guarde el valor convertido*/
float promedio=0;
float muestras=0;
float sumatoriamuestras=0;



/*==================[internal data definition]===============================*/


/*void LecturaUart(){

}*/
void Promediomuestras(){/*Función que calcula el promedio*/
  promedio=sumatoriamuestras/muestras;
}

void FuncionADC(){/*Interrupción ADC*/
	AnalogInputRead(CH3,&valor);/*Lectura de datos del CH3*/
	sumatoriamuestras= sumatoriamuestras+valor;
	adc=TRUE;/*Cambio de bandera de conversión AD, activada*/

}


void FuncionamientoTimer(){/*Interrupción del timer*/
	AnalogStartConvertion();/*Inicio de conversión analógica*/
	muestras++;

}




/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void){

	SystemClockInit();/*Inicializo clock del CPU*/
	LedsInit(); /*Inicializo leds*/



	timer_config mi_ACD={TIMER_A,67,FuncionamientoTimer}; /*Configuro struct con timer, periodo 1/15Hz y función que quiero que ejecute*/
	TimerInit(&mi_ACD); /*Inicializo timer*/
	TimerStart(mi_ACD.timer);/*Comienza a contar el timer*/

	serial_config mi_uart ={SERIAL_PORT_PC, BAUDIO, NO_INT}; /*Configuro struct de uart*/
	UartInit(&mi_uart);/*Inicializo Uart*/

	analog_input_config conf_analog ={CH3, AINPUTS_SINGLE_READ,FuncionADC};/*Configuro struct de analogico*/
	AnalogInputInit(&conf_analog);/*Inicializo input*/
	AnalogOutputInit();/*Inicializo output*/




while (1) {
	if (adc ==TRUE){

		if(muestras==15){
			Promediomuestras();
			UartSendString(SERIAL_PORT_PC, UartItoa(promedio, 10));
			UartSendString(SERIAL_PORT_PC,"\r\n");
			muestras=0;
			promedio=0;
			sumatoriamuestras=0;
		}


		adc=FALSE;
				/*Se muestran por la UART el valor promedio cada 1s*/
	}/*FIN ADC*/

}/*FIN DEL WHILE*/
}/*FIN DEL MAIN*/

/*==================[end of file]============================================*/

