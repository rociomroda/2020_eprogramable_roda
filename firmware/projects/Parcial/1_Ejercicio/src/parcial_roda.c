/*! @mainpage parcial_roda
 *
 * \section genDesc General Description
 *
 *Mediante el Tcrt5000 le cuentan las cantidades de aberturas.
 *Se cuentan esas cantidades de aberturas  y mediante el timer implementado, a los 60s se ve que cantidad de aberturas hay en el contador.
 *Si en 60s hay entre 800 y 1200 aberturas, la UART nos mostrará "OK" y se encenderá el LED 3
 *Si en 60s hay menos de 800 aberturas,  la UART nos mostrará "SLOW" y se encenderá el LED 1
 * Si en 60s hay más de 1200 aberturas,  la UART nos mostrará "FAST" y se encenderá el LED 2
 *
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	OUT 	    | 	TCOL0	    |
 * |    GND         |   GND         |
 * |    VCC         |   5 V         |
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 02/11/2020 | Creación del document                          |
 * | 02/11/2020 | Versión final                                  |
 *
 *
 * @author Roda Rocío Milagros
 *
 */

/*==================[inclusions]=============================================*/
#include "../../1_Ejercicio/inc/parcial_roda.h"       /* <= own header */

#include "systemclock.h"
#include "led.h"
#include "bool.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"
#include "Tcrt5000.h"
/*==================[macros and definitions]=================================*/
#define OBJET 1
#define NOBJET 0
#define COUNT_DELAY 3000000
#define FALSE 0
#define TRUE 1
#define BAUDIO 115200



uint8_t contadortimer=0;/*guarda las veces que corre el timer*/
uint8_t velocidad=0;/*cuenta las ranuras*/


/*==================[internal data definition]===============================*/


void LecturaUart(){/*Lee un dato enviado por la consola*/

}
void FuncionamientoTimer(){
	contadortimer++;
}
/*Las funciones Tecla1,Tecla2,Tecla3 indican lo que hace el programa de acuerdo a la interrupción que ocurre*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	bool estado=NOBJET; /*Variable que guarda el estado del detector de objetos*/
	bool estadoant=NOBJET;/*Variable que guarda el estado anterior del detector de objetos*/
	SystemClockInit();/*Inicializo clock del CPU*/
	LedsInit(); /*Inicializo leds*/
	SwitchesInit();/*Inicializo las teclas*/
	Tcrt5000Init(GPIO_T_COL0);/*Inicializo el puerto donde está conectado mi dispositivo externo*/
	/*SwitchActivInt ejecuta la interrupción de acuerdo a la tecla que es presionada y el programa ejecuta lo que corresponde a esa interrupción (lo que ocurre es delimitado en la función enviada por puntero) */
	timer_config mi_timer={TIMER_A, 1000,FuncionamientoTimer}; /*Configuro struct con timer, periodo y función que quiero que ejecute*/
	TimerInit(&mi_timer); /*Inicializo timer*/
	serial_config mi_uart ={SERIAL_PORT_PC, BAUDIO, LecturaUart}; /*Configuro struct de uart*/
	UartInit(&mi_uart);/*Inicializo Uart*/
	while (1) {
			TimerStart(mi_timer.timer);
			estado = Tcrt5000State();/*Leo y guardo el estado del puerto del Tcrt5000*/
			if ((estado == OBJET) && (estadoant == NOBJET)) {
				/*Si el estado es 1, detecta un objeto y lo suma al contador de objetos*/
				velocidad = velocidad + 1;
			}
			if(contadortimer==60){
				if((velocidad>799) && (velocidad<1201)){ /*Pasé los rpm a aberturas(40rpm*20aberturas=800cuentas y 60rpm*aberturas=1200)*/
					LedOn(LED_3);
					UartSendString(SERIAL_PORT_PC," ");
					UartSendString(SERIAL_PORT_PC,"OK");
					UartSendString(SERIAL_PORT_PC,"\r\n");
				}
				if(velocidad<800){
					LedOn(LED_1);
					UartSendString(SERIAL_PORT_PC," ");
					UartSendString(SERIAL_PORT_PC,"SLOW");
					UartSendString(SERIAL_PORT_PC,"\r\n");

				}
				if(velocidad>1200){
					LedOn(LED_2);
					UartSendString(SERIAL_PORT_PC," ");
					UartSendString(SERIAL_PORT_PC,"FAST");
					UartSendString(SERIAL_PORT_PC,"\r\n");
				}
				contadortimer=0;
				velocidad=0;

			}
			LedOff(LED_1);
			LedOff(LED_2);
			LedOff(LED_3);

			estadoant = estado;/*Le coloca a la variable estado anteror, el valor actual que lee el puerto*/



    }

}


    


/*==================[end of file]============================================*/

