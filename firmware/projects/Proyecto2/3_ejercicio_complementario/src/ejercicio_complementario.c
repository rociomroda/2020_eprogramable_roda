 /*! @mainpage Ejercicio_Complementario
 *
 * \section genDesc General Description
 *
 *https://youtu.be/hXC1S3V84P4
 *
 *Object counter starts when switch 1 is true.
 *Leds show the number of count in binary and they are off when the counter is zero or when switch 1 is false.
 *When switch 1 is true and switch 2 too, leds show the last count before push switch 2, but the counter continuous counting objects
 *When switch 3 is true, the counter returns to zero and starts again.
 *When switch 1 is false, all leds are off and counter isn't count.
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	OUT 	    | 	TCOL0	    |
 * |    GND         |   GND         |
 * |    VCC         |   5 V         |
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 13/09/2020 | Document creation		                         |
 * | 09/09/2020 | Functionalities were added                     |
 * | 18/09/2020 | New functionalities. Last Version              |
 *
 * @author Roda Rocío Milagros
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/ejercicio_complementario.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "bool.h"
#include "Tcrt5000.h"
#include "switch.h"
/*==================[macros and definitions]=================================*/
#define OBJET 1
#define NOBJET 0
#define COUNT_DELAY 3000000

/*==================[internal data definition]===============================*/
void Delay(void)
{
	uint32_t i;

	for(i=COUNT_DELAY; i!=0; i--)
	{
		   asm  ("nop");
	}
}

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	uint8_t contador=0; /*Creo una variable que cuente la cantidad de oobjetos detectados*/
	uint8_t tec, tecantes; /*Variables para los estados actuales y anteriores de teclas*/
	uint8_t contadorant; /*Variable que guarda el estado anterior del contador*/
	bool estado=NOBJET; /*Variable que guarda el estado del detector de objetos*/
	bool estadoant=NOBJET;/*Variable que guarda el estado anterior del detector de objetos*/
	SystemClockInit();/*Inicializo clock del CPU*/
	LedsInit(); /*Inicializo leds*/
	SwitchesInit();/*Inicializo las teclas*/
	Tcrt5000Init(GPIO_T_COL0);/*Inicializo el puerto donde está conectado mi dispositivo externo*/
	bool tecla1=NOBJET;
	bool tecla2=NOBJET;
	bool tecla3=NOBJET;
	/*Variables tecla1,tecla2,tecla3, son banderas para guardar estados de teclas*/
	while (1) {
		tec = SwitchesRead();/*Leo el estado de las tecla presionada*/

		if (tec != tecantes) { /*Si el estado de la tecla es distinto al estado anterior de la tecla, entra al if*/
			switch (tec) { /*Switch que dependiendo de la tecla presionada (1,2 o 3) le canbia el estado a las mismas*/
			case (SWITCH_1):
				tecla1 = !tecla1;
				break;
			case (SWITCH_2):
				tecla2 = !tecla2;
				break;
			case (SWITCH_3):
				tecla3 = !tecla3;
				break;

			}

		}
		tecantes = tec; /*Le pongo al valor anterior de la tecla, el valor actual de la tecla*/
		if (tecla1 == true) { /*Si presiono por primera vez la tecla 1, se prende el contador (entra a este if)*/
			estado = Tcrt5000State();/*Leo y guardo el estado del puerto del Tcrt5000*/
			if ((estado == OBJET) && (estadoant == NOBJET)) { /*Si el estado es 1, detecta un objeto y lo suma al contador de objetos*/
				contador = contador + 1;
				if (tecla2 == false) {/*Si el estado de la tecla 2 es bajo, el estado anterior del contador va a ser igual que el actual, sino, los los led mostrados quedan en el ultimo estado antes de presionarla*/
					contadorant = contador;

				}
			}

			if (tecla3 == true) {/*Vuelve el contador a 0*/
				Delay();
				contador = 0;
				contadorant = contador;
				tecla3 = false;

			}
			LedsMask(contadorant);/*Muestra el resultado del contador en numero binario*/

			estadoant = estado;/*Le coloca a la variable estado anteror, el valor actual que lee el puerto*/
		} else {/*Si la tecla está en bajo (cero), se apagan todos los leds y el contador vuelve a cero*/
			LedOff(LED_1);
			LedOff(LED_2);
			LedOff(LED_3);
			LedOff(LED_RGB_R);
			LedOff(LED_RGB_B);
			LedOff(LED_RGB_G);
			contador = 0;
			contadorant = contador;

		}

	}

}

    


/*==================[end of file]============================================*/

