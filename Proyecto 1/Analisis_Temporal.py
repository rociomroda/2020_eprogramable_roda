# -*- coding: utf-8 -*-

"""

Sistemas de Adquisición y Procesamiento de Señales
Facultad de Ingeniería - UNER

Análisis Temporal:
    En el siguiente script se ejemplifica el proceso de carga de una señal a 
    partir de una archivo de audio y el cálculo de la duración temporal de los 
    eventos em la misma.

Autor: Albano Peñalva
Fecha: Agosto 2020

"""

# Librerías
from scipy import signal
from scipy.io import wavfile
import numpy as np
import matplotlib.pyplot as plt
from envolvente import envolvente

plt.close('all') # cerrar gráficas anteriores

# %% Lectura del archivo de audio 

filename = '1_silbido.wav'          # nombre de archivo
fs, data = wavfile.read(filename)   # frecuencia de muestreo y datos de la señal

# Definición de parámetro temporales
ts = 1 / fs                     # tiempo de muestreo
N = len(data)                   # número de muestras en el archivo de audio
t = np.linspace(0, N * ts, N)   # vector de tiempo
senial = data#[:, 1]            # se extrae un canal de la pista de audio (si el audio es estereo)
senial = senial * 3.3 / (2 ** 16 - 1) # se escala la señal a voltios (considerando un CAD de 16bits y Vref 3.3V)

# %% Detección de envolvente 

senial_env = envolvente(senial, 50)

# %% Duración y Localización del silbido 

maximo = np.max(senial_env)

# Detección de los picos
picos, _ = signal.find_peaks(senial_env, prominence=maximo/4)

# Medición del ancho y posición de los picos
anchos = signal.peak_widths(senial_env, picos, rel_height=0.5)

print("La duración del silbido es de {:.2f} seg.".format(anchos[0][0]*ts))
print("El silbido comienza a los {:.2f} seg y finaliza a los {:.2f} seg.".format(anchos[2][0]*ts,
                                                                                 anchos[3][0]*ts))
# %% Graficación 

# Se crea una gráfica 
fig2, ax2 = plt.subplots(1, 1, figsize=(20, 10))
fig2.suptitle('Silbido', fontsize=18)

# Se grafica la señal, su envolvente y se señala la duración del silbido
ax2.plot(t, senial, label='Señal de audio', zorder=1, color='blue')
ax2.plot(t, senial_env, label='Envolvente', zorder=2, color='green')
ax2.plot(t[picos], senial_env[picos], "X", label='Picos', 
         zorder=3, color='red')
ax2.hlines(anchos[1], anchos[2] * ts, anchos[3] * ts, label='Duración Silbido', 
          zorder=4, color="orange")
ax2.set_xlabel('Tiempo [s]', fontsize=15)
ax2.set_ylabel('Tensión [V]', fontsize=15)
ax2.set_xlim([0, ts*N])
ax2.grid()
ax2.legend(fontsize=12)
plt.show()
