/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JM Reta - jmreta@ingenieria.uner.edu.ar
 * Eduardo Filomena
 * Gonzalo Cuenca
 * Juan Ignacio Cerrudo
 * Albano Peñalva
 * Sebastián Mateos
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>


/*==================[macros and definitions]=================================*/
#define ON 1
#define OFF 0
#define TOGGLE 2



/*==================[internal functions declaration]=========================*/

struct leds {
	uint8_t n_led;       /* indica el número de led a controlar*/
	uint8_t n_ciclos;   /*indica la cantidad de ciclos de encendido/apagado*/
	uint8_t periodo;    /*indica el tiempo de cada ciclo*/
	uint8_t mode;       /*ON, OFF, TOGGLE*/

}my_leds;

void ControlarLeds(struct leds *mis_leds){
	uint8_t j=0;
	switch (mis_leds->mode){
	case ON:
		switch(mis_leds->n_led){
		case 1:
			printf("Led 1: encendido \r\n");
			break;
		case 2:
			printf("Led 2: encendido \r\n");
			break;
		case 3:
			printf("Led 3: encendido \r\n");
			break;
		}
		break;
		case OFF:
			switch(mis_leds->n_led){
			case 1:
				printf("Led 1: apagado \r\n");
				break;
			case 2:
				printf("Led 2: apagado \r\n");
				break;
			case 3:
				printf("Led 3: apagado \r\n");
				break;
			}
			break;
		case TOGGLE:

			for(j=0;j <mis_leds->n_ciclos;j++){
				switch(mis_leds->n_led){
						case 1:
							printf("Led 1: toggle \r\n");
							break;
						case 2:
							printf("Led 2: toggle \r\n");
							break;
						case 3:
							printf("Led 3: toggle \r\n");
							break;
						}
				uint8_t n;

				for (n=0; n<mis_leds->periodo; n++){
					printf("Retardo \r\n");
				}
			}

	}
}
int main(void)
{
	my_leds.mode=TOGGLE;
	my_leds.n_ciclos=2;
	my_leds.n_led=1;
	my_leds.periodo=10;

	ControlarLeds(&my_leds);
	return 0;
}

/*==================[end of file]============================================*/

