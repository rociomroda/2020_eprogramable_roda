/*! @mainpage riego_inteligente
 *
 * @section genDesc General Description
 *
 *
 *
 *Mediante RTC de configura la fecha actual. Luego se comprueba en la estación del año.
 *
 *Sensor analógico de humedad envia datos mediante CH1. Sensor analógico de luz (LDR) envía datos mediante CH2.
 *
 *Cada un numero de muestras establecido, promedia las muestras y chequea el nivel de humedad en base al promedio.
 *
 *Si ese promedio es menor a 512(corresponde al 50% de humedad) se chequea el nivel de luz.
 *
 *Si el nivel de luz es bajo, se chequea la hora actual. Si la hora está dentro de los parámetros de regado de la estación se riega.
 *
 *Se riega por un intervalo de tiempo determinado y se vuelve a chequear los niveles de humedad. Si son menores a 307 (70% de humedad aprox) se riega otra vez.
 *
 *La escala de humedad es la siguiente: 0 corresponde a 100% de humedad y 1023 a 0%
 *
 *En el LDR cuando hay mucha luz, la resistencia el baja por ende el valor maximo está cerca de 1023. En la oscuridad es al revés. Tomamos como poca luz el dato 400.
 *
 *Tomamos ese dato de 400 debido al supuesto uso del LDR modelo GL5528.
 *
 *
 *
 *
 *
 * @section hardConn Hardware Connection
 *
 * | Sensor humedad |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	OUT 	    | 	CH1	        |
 * |    GND         |   GNDA        |
 * |    VCC         |   VDDA        |
 *
 * | Sensor LDR     |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	OUT 	    | 	CH2	        |
 * |    GND         |   GNDA        |
 * |    VCC         |   VDDA        |
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 26/10/2020 | Creacion del documento		                 |
 *
 * @author Roda Rocío Milagros
 *
 */

#ifndef _RIEGO_INTELIGENTE_H
#define _RIEGO_INTELIGENTE_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _RIEGO_INTELIGENTE_H */

