/*! @mainpage mi_timer_uart
 *
 * \section genDesc General Description
 *
 * Video de funcionamiento: https://youtu.be/K8q7SMxC2yM
 *
 * This section describes how the program works.
 *
 *Object counter and timer start when switch 1 is true.
 *LEDs show the number of count in binary and they are off when the counter is zero or when switch 1 is false.
 *When switch 1 is true and switch 2 too, LEDs show the last count before push switch 2, but the counter continuous counting objects
 *When switch 3 is true, the counter returns to zero and starts again.
 *When switch 1 is false, LEDs reset and turn off.Counter and timer reset and stop.
 *LEDs and counter are refreshing every 1 second by timer functionality. In other words, LEDs and counter have a retard of 1 second.
 *Serial connection with computer keyboard by UART. If you push letter 'o' (lower case or upper case), objects counter and timer start. LEDs and user interface show value of counter.
 *If you push letter 'h' (lower case or upper case) after you pushed letter 'o', LEDs and user interface show the last count before push letter 'h'.
 *If you push number 0, counter starts again. LEDs and user interface too.
 *If you push letter 'o' again, counter and timer reset and stop. User interface stops. LEDs reset and turn off.
 *
 * \section hardConn Hardware Connection
 *
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	OUT 	    | 	TCOL0	    |
 * |    GND         |   GND         |
 * |    VCC         |   5 V         |
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 03/10/2020 | Document creation		                         |
 *
 * @author Roda Rocío Milagros
 *
 */


#ifndef _MI_TIMER_UART_H
#define _MI_TIMER_UARTH


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _MI_TIMER_UART_H */

