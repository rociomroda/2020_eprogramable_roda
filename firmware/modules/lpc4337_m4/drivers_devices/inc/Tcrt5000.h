/* Copyright 2020,
 * Roda Rocío
 * rocioroda96@gmail.com
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */


#ifndef TCRT5000_H
#define TCRT5000_H
/** @brief Bare Metal header for Tcrt5000 in EDU-CIAA NXP
 **
 ** This is a driver for Tcrt5000 IR sensor mounted on pin out T_COL0
 **
 **/

/** @addtogroup CIAA_Firmware CIAA Firmware
 ** @{ */

/** @addtogroup Sources_LDM Leandro D. Medus Sources
 ** @{ */
/** @addtogroup Baremetal Bare Metal source file
 ** @{ */


/*
 * Version 0.1 all made by Roda Rocío

 */

/*==================[inclusions]=============================================*/
#include "bool.h"
#include <stdint.h>
#include "gpio.h"

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/


/*==================[external functions declaration]=========================*/
/** @brief Initialization function to control the port dout in the EDU-CIAA BOARD
 * This function initialize the port dout present in the EDU-CIAA board
 * @param[in] gp Indicates to which port present in the EDU-CIAA board will be initialized
 * @return true if no error
 */
bool Tcrt5000Init(gpio_t dout);

/** @brief Function to read the state of output in Tcrt5000 IR sensor
 * The function returns bool , where true represent an obstacle recorder by the infrared receptor.
 * @return false if no obstacle recorder by the infrared receptor.
 */
bool Tcrt5000State(void);

/**@brief Deinitialization function to control the port dout in the EDU-CIAA BOARD
 * This function deinitialize the port dout present in the EDU-CIAA board
 * @param[in] gp Indicates to which port present in the EDU-CIAA board will be deinitialized
 */
bool Tcrt5000Deinit(gpio_t dout);

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* #ifndef TCRT5000_H */

