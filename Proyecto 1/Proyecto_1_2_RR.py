# -*- coding: utf-8 -*-
"""
Created on Tue Mar 16 11:16:15 2021

@author: Rocío Roda
"""

#Librerias

from scipy.fft import fftfreq, fftshift
from scipy import fft
import matplotlib.pyplot as plt
from scipy.io import wavfile
import numpy as np
import matplotlib.pyplot as plt

plt.close('all') # cerrar gráficas anteriores

filename= ['Puritan_Bennett_840_UTI.wav','Puritan_Bennett_840.wav','Newport_HT50_UTI.wav','Newport_HT50.wav', 'Dragger_Carina_UTI.wav','Dragger_Carina.wav','Alarmas.wav','Alarmas_UTI.wav'] #Creacion de una lista con noimbres de los archivos

n= len(filename)

for n in filename:
    fs,data = wavfile.read(n)
    # Definición de parámetro temporales
    ts = 1 / fs                     # tiempo de muestreo
    N = len(data)                   # número de muestras en el archivo de audio
    t = np.linspace(0, N * ts, N)   # vector de tiempo
    senial = data[:, 1]            # se extrae un canal de la pista de audio (si el audio es estereo)
    senial = senial * 3.3 / (2 ** 16 - 1) # se escala la señal a voltios (considerando un CAD de 16bits y Vref 3.3V)
    freq = fft.fftfreq(N, d=1/fs)   # se genera el vector de frecuencias
    senial_fft = fft.fft(senial)    # se calcula la transformada rápida de Fourier

    # El espectro es simétrico, nos quedamos solo con el semieje positivo
    f = freq[np.where(freq >= 0)]      
    senial_fft = senial_fft[np.where(freq >= 0)]

    # Se calcula la magnitud del espectro
    senial_fft_mod = np.abs(senial_fft) / N     # Respetando la relación de Parceval
    # Al haberse descartado la mitad del espectro, para conservar la energía 
    # original de la señal, se debe multiplicar la mitad restante por dos (excepto
    # en 0 y fm/2)
    senial_fft_mod[1:len(senial_fft_mod-1)] = 2 * senial_fft_mod[1:len(senial_fft_mod-1)]
    # Se crea una gráfica que contendrá dos sub-gráficos ordenados en una fila y dos columnas
    fig1, ax1 = plt.subplots(1, 2, figsize=(20, 10))
    fig1.suptitle(n, fontsize=18)

    # Se grafica la señal temporal
    ax1[0].plot(t, senial)
    ax1[0].set_xlabel('Tiempo [s]', fontsize=15)
    ax1[0].set_ylabel('Tensión [V]', fontsize=15)
    ax1[0].set_title('Señal temporal', fontsize=15)
    ax1[0].set_xlim([0, ts*N])
    ax1[0].grid()

    # se grafica la magnitud de la respuesta en frecuencia
    ax1[1].plot(f, senial_fft_mod)
    ax1[1].set_xlabel('Frecuencia [Hz]', fontsize=15)
    ax1[1].set_ylabel('Magnitud [V]', fontsize=15)
    ax1[1].set_title('Magnitud de la Respuesta en Frecuencia', fontsize=15)
    ax1[1].set_xlim([0, fs/2])
    ax1[1].grid()

    plt.show()


