########################################################################
#Cátedra: Electrónica Programable
#FIUNER
#2018
#Autor/es:
#JMreta - jmreta@ingenieria.uner.edu.ar
#
#Revisión:
########################################################################

# Ruta del Proyecto
####Ejemplo 1: Hola Mundo
##Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = 1_hola_mundo
#NOMBRE_EJECUTABLE = hola_mundo.exe

####Ejemplo 2: Control Mundo
#PROYECTO_ACTIVO = 2_control_mundo
#NOMBRE_EJECUTABLE = control_mundo.exe

####Ejemplo 3: Promedio
#PROYECTO_ACTIVO = 3_promedio_1
#NOMBRE_EJECUTABLE = promedio.exe

#PROYECTO_ACTIVO = 5_menu
#NOMBRE_EJECUTABLE = menu.exe

#PROYECTO_ACTIVO = Ej_c_d_e
#NOMBRE_EJECUTABLE = c_d_e.exe

###Ejercicio1
#PROYECTO_ACTIVO = ejercicio_1
#NOMBRE_EJECUTABLE = ejercicio1.exe

###Ejercicio7
#PROYECTO_ACTIVO = ejercicio_7
#NOMBRE_EJECUTABLE = ejercicio7.exe

###Ejercicio9
#PROYECTO_ACTIVO = ejercicio_9
#NOMBRE_EJECUTABLE = ejercicio9.exe

###Ejercicio12
#PROYECTO_ACTIVO = ejercicio_12
#NOMBRE_EJECUTABLE = ejercicio12.exe

###Ejercicio14
#PROYECTO_ACTIVO = ejercicio_14
#NOMBRE_EJECUTABLE = ejercicio14.exe

###Ejercicio16
#PROYECTO_ACTIVO = ejercicio_16
#NOMBRE_EJECUTABLE = ejercicio16.exe

###Ejercicio17
#PROYECTO_ACTIVO = ejercicio_17
#NOMBRE_EJECUTABLE = ejercicio17.exe

###Ejerciciointegradora
#PROYECTO_ACTIVO = integrador_a
#NOMBRE_EJECUTABLE = integradora.exe

###Ejerciciointegradorcd
PROYECTO_ACTIVO = integrador_cd
NOMBRE_EJECUTABLE = integradorcd.exe