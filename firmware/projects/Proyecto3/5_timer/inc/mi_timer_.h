/*! @mainpage mi_timer
 *
 * \section genDesc General Description
 *
 * [INSERT VIDEO]
 *
 * This section describes how the program works.
 *
 *Object counter and timer start when switch 1 is true.
 *LEDs show the number of count in binary and they are off when the counter is zero or when switch 1 is false.
 *When switch 1 is true and switch 2 too, LEDs show the last count before push switch 2, but the counter continuous counting objects
 *When switch 3 is true, the counter returns to zero and starts again.
 *When switch 1 is false, LEDs reset and turn off.Counter and timer reset and stop.
 *LEDs and counter are refreshing every 1 second by timer functionality. In other words, LEDs and counter have a retard of 1 second.
 *
 * \section hardConn Hardware Connection
 *
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	OUT 	    | 	TCOL0	    |
 * |    GND         |   GND         |
 * |    VCC         |   5 V         |
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 27/09/2020 | Document creation		                         |
 *
 * @author Roda Rocío Milagros
 *
 */


#ifndef _MI_TIMER_H
#define _MI_TIMER_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _MI_TIMER_H */

