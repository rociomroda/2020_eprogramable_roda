/*! @mainpage osciloscopio_digital
 *
 * \section genDesc General Description
 *
 *Video de funcionamiento: https://drive.google.com/file/d/12e_fI-Rxz8hnKlvCvmPGl2zNcNFM1nbk/view?usp=sharing
 *
 *Disparo de conversión AD con interrupción periódica de timer de 1/500Hz (2ms).
 *
 *Obtención de datos analógicos mediante el CH1 y se los convierte a digitales, es controlada por el timer. Cuando está lista la conversión, se sacan los valores por la UART y el graficador en serie.
 *
 *Muestreo de señal de ECG por la UART y graficador en serie que previamente es convertida de digital a analógica.
 *
 *La misma señal de ECG es mostrada 2 veces en el graficador pero si se apreta la TECLA 1, a una de ellas se le aplica un filtro pasabajos de 20Hz de frecuencia.
 *Si se aprieta la TECLA2, se deshabilita el filtro, si se aprieta TECLA 3 baja la frecuencia de corte en 5 unidades, la cota de la misma es de 20 Hz.
 *Si se aprieta la TECLA4, aumenta la frecuencia de corte en 5 unidades. La cota superior de frecuencia de corte es de 60Hz.
 *
 *
 * \section hardConn Hardware Connection
 *
 * | Potenciometro  |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	OUT 	    | 	CH1	        |
 * |    GND         |   GNDA        |
 * |    VCC         |   DAC         |
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 10/10/2020 | Creación del documento		                 |
 *
 * @author Roda Rocío Milagros
 *
 */


#ifndef _OSCILOSCOPIO_DIGITAL_H
#define _OSCILOSCOPIO_TIMER_UART_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _OSCILOSCOPIO_DIGITAL_H */

