# -*- coding: utf-8 -*-
"""
Created on Wed March 10 12:46 p.m 2021

@author: RR
"""
from scipy import fft
from scipy.io import wavfile
import numpy as np
import matplotlib.pyplot as plt
plt.close('all') # cerrar gráficas anteriores
#%%Lectura de archivo wav 
filename='Alarmas.wav'
fsalarmas, alarmas = wavfile.read(filename) #Frec de muestreo y datos de Alarma
filename='Alarmas_UTI.wav'
fsalarmuti, alarmasuti = wavfile.read(filename) #Frec de muestreo y datos de Alarma en la UTI
filename='Dragger_Carina.wav'
fscarina, carina= wavfile.read(filename) # Frec de muestreo y datos de dispositivo Dragger Carina
filename='Dragger_Carina_UTI.wav'
fscarinauti, carinauti= wavfile.read(filename)  # Frec de muestreo y datos de dispositivo Dragger Carina con sonidos de UTI
filename='Newport_HT50.wav'
fsnewport, newport= wavfile.read(filename) #Frec de mustreo y datos dispositivo Newport HT50
filename='Newport_HT50_UTI.wav'
fsnewportuti, newportuti = wavfile.read(filename) #Frec de mustreo y datos dispositivo Newport HT50 con sonidos de UTI
filename='Puritan_Bennett_840.wav'
fspuritan, puritan= wavfile.read(filename) #Frec de mustreo y datos dispositivo Puritan Bennett 840
filename='Puritan_Bennett_840_UTI.wav'
fspuritanuti, puritanuti= wavfile.read(filename) #Frec de mustreo y datos dispositivo Puritan Bennett 840 con sonidos de UTI
filename='UTI.wav'
fsuti, uti=wavfile.read(filename) #Frec de mustreo y datos de ambiente de UTI
 
#%% Definición de parámetros temporales

#Alarmas

tsa = 1 / fsalarmas              # tiempo de muestreo de alarmas
Na = len(alarmas)               # número de muestras en el archivo de audio Alarmas.wav
ta = np.linspace(0, Na * tsa, Na)  # vector de tiempo
senialalarmas = alarmas [:, 1]            # se extrae un canal de la pista de audio porque es formato estéreo
senialalarmas = senialalarmas  * 3.3 / (2 ** 16 - 1) # se escala la señal a voltios (considerando un CAD de 16bits y Vref 3.3V)


#Alarmas UTI

tsauti = 1 / fsalarmuti           # tiempo de muestreo de alarmas UTI
Nauti = len(alarmasuti)              # número de muestras en el archivo de audio Alarmas_UTI.wav
tauti = np.linspace(0, Nauti * tsauti, Nauti)  # vector de tiempo
senialalarmasuti = alarmasuti [:, 1]            # se extrae un canal de la pista de audio porque es formato estéreo
senialalarmasuti = senialalarmasuti  * 3.3 / (2 ** 16 - 1) # se escala la señal a voltios (considerando un CAD de 16bits y Vref 3.3V)

#Dragger Carina
tscarina = 1 /fscarina             # tiempo de muestreo de Dragger Carina
Ncarina = len(carina)              # número de muestras en el archivo de audio Dragger_Carina.wav
tcarina = np.linspace(0, Ncarina * tscarina, Ncarina)  # vector de tiempo
senialcarina = carina [:, 1]            # se extrae un canal de la pista de audio porque es formato estéreo
senialcarina = senialcarina  * 3.3 / (2 ** 16 - 1) # se escala la señal a voltios (considerando un CAD de 16bits y Vref 3.3V)

#Dragger Carina UTI
tscarinauti = 1 /fscarinauti              # tiempo de muestreo de Dragger Carina
Ncarinauti = len(carinauti)              # número de muestras en el archivo de audio Dragger_Carina.wav
tcarinauti = np.linspace(0, Ncarinauti * tscarinauti, Ncarinauti)  # vector de tiempo
senialcarinauti = carinauti [:, 1]            # se extrae un canal de la pista de audio porque es formato estéreo
senialcarinauti = senialcarinauti  * 3.3 / (2 ** 16 - 1) # se escala la señal a voltios (considerando un CAD de 16bits y Vref 3.3V)

#Newport HT50
tsnewport = 1 /fsnewport              # tiempo de muestreo de Newport HT50
Nnewport = len(newport)              # número de muestras en el archivo de audio Newport_HT50.wav
tnewport = np.linspace(0, Nnewport * tsnewport, Nnewport)  # vector de tiempo
senialnewport = newport [:, 1]            # se extrae un canal de la pista de audio porque es formato estéreo
senialnewport = senialnewport  * 3.3 / (2 ** 16 - 1) # se escala la señal a voltios (considerando un CAD de 16bits y Vref 3.3V)

#Newport HT50 con ruido de UTI
tsnewportuti = 1 /fsnewportuti              # tiempo de muestreo de Newport HT50 con ruido de UTI
Nnewportuti = len(newportuti)              # número de muestras en el archivo de audio Newport_HT50_UTI.wav
tnewportuti = np.linspace(0, Nnewportuti * tsnewportuti, Nnewportuti)  # vector de tiempo
senialnewportuti = newportuti[:, 1]            # se extrae un canal de la pista de audio porque es formato estéreo
senialnewportuti = senialnewportuti  * 3.3 / (2 ** 16 - 1) # se escala la señal a voltios (considerando un CAD de 16bits y Vref 3.3V)

#Puritan Bennett 840 
tspuritan = 1 /fspuritan              # tiempo de muestreo de Puritan Bennett 840
Npuritan = len(puritan)              # número de muestras en el archivo de audio Puritan_Bennett_840.wav
tpuritan = np.linspace(0, Npuritan * tspuritan , Npuritan)  # vector de tiempo
senialpuritan = puritan[:, 1]            # se extrae un canal de la pista de audio porque es formato estéreo
senialpuritan = senialpuritan * 3.3 / (2 ** 16 - 1) # se escala la señal a voltios (considerando un CAD de 16bits y Vref 3.3V)

#Puritan Bennett 840 con ruido de UTI
tspuritanuti = 1 /fspuritanuti              # tiempo de muestreo de Puritan Bennett 840 con ruido de UTI
Npuritanuti = len(puritanuti)              # número de muestras en el archivo de audio Puritan_Bennett_840_UTI.wav
tpuritanuti = np.linspace(0, Npuritanuti * tspuritanuti , Npuritanuti)  # vector de tiempo
senialpuritanuti = puritanuti[:, 1]            # se extrae un canal de la pista de audio porque es formato estéreo
senialpuritanuti = senialpuritanuti * 3.3 / (2 ** 16 - 1) # se escala la señal a voltios (considerando un CAD de 16bits y Vref 3.3V)

#UTI
tsuti = 1 /fsuti             # tiempo de muestreo de UTI
Nuti = len(uti)              # número de muestras en el archivo de audio UTI.wav
tuti = np.linspace(0, Nuti * tsuti , Nuti)  # vector de tiempo
senialuti = uti[:, 1]            # se extrae un canal de la pista de audio porque es formato estéreo
senialuti = senialuti * 3.3 / (2 ** 16 - 1) # se escala la señal a voltios (considerando un CAD de 16bits y Vref 3.3V)

#%%Cálculo de transformada de Fourier

#Alarmas 

freq=fft.fftfreq(Na, d=1 / fsalarmas)   # se genera el vector de frecuencias
alarmas_fft =fft.fft(senialalarmas)    # se calcula la transformada rápida de Fourier

# El espectro es simétrico, nos quedamos solo con el semieje positivo
fa= freq[np.where(freq >= 0)]      
alarmas_fft = alarmas_fft[np.where(freq >= 0)]

# Se calcula la magnitud del espectro
alarmas_fft_mod = np.abs(alarmas_fft) / Na     # Respetando la relación de Parceval
# Al haberse descartado la mitad del espectro, para conservar la energía 
# original de la señal, se debe multiplicar la mitad restante por dos (excepto
# en 0 y fm/2)
alarmas_fft_mod[1:len(alarmas_fft_mod-1)] = 2 * alarmas_fft_mod[1:len(alarmas_fft_mod-1)]