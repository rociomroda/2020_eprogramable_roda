/*! @mainpage parcial_roda
 *
 * \section genDesc General Description
 *
 *
 *
 * This section describes how the program works.
 *
 *Mediante el Tcrt5000 le cuentan las cantidades de aberturas.
 *Se cuentan esas cantidades de aberturas  y mediante el timer implementado, a los 60s se ve que cantidad de aberturas hay en el contador.
 *Si en 60s hay entre 800 y 1200 aberturas, la UART nos mostrará "OK" y se encenderá el LED 3
 *Si en 60s hay menos de 800 aberturas,  la UART nos mostrará "SLOW" y se encenderá el LED 1
 * Si en 60s hay más de 1200 aberturas,  la UART nos mostrará "FAST" y se encenderá el LED 2
 *
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	OUT 	    | 	TCOL0	    |
 * |    GND         |   GND         |
 * |    VCC         |   5 V         |
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 02/11/2020 | Creación del document                          |
 * | 02/11/2020 | Versión final                                  |
 *
 * @author Roda Rocío Milagros
 *
 */


#ifndef _PARCIAL_RODA_H
#define _PARCIAL_RODA_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _PARCIAL_RODA_H */

