# -*- coding: utf-8 -*-
"""
Created on Sun Mar 14 10:36:59 2021

@author: Rocío Roda
"""
#Librerias

from scipy.fft import fftfreq, fftshift
from scipy import fft
import matplotlib.pyplot as plt
from scipy.io import wavfile
import numpy as np
import matplotlib.pyplot as plt

plt.close('all') # cerrar gráficas anteriores

#%% Archivo UTI
#Lectura del archivo de audio 

filename='UTI.wav' #Nombre del archivo
fsuti, uti=wavfile.read(filename) #Frecuencia de muestreo y datos de señal

#Definición de parámetros temporales

tsuti=1/fsuti #Tiempo de muestreo
Nuti=len(uti) #Numero de muestras
tuti=np.linspace(0, Nuti * tsuti , Nuti) #Vector de tiempos
senialuti= uti [:, 1] #Extraemos solo una pista del audio porque es estéreo
senialuti= senialuti* 3.3 / (2 ** 16 - 1) #Escalamos la señal a voltios suponiendo que se obtuvo con un CAD de 16bits y Vref 3.3V

#Cálculo de transformada de fourier

frequti= fftfreq(Nuti, d=1/fsuti) #generación del espectro de frecuencias
senial_fftuti= fft.fft(senialuti) #se calcula la Tansformada Rápida de Fourier

#Al ser simétrico el espectro nos quedamos solo con la parte positiva
futi=frequti[np.where(frequti>=0)]
senial_fftuti=senial_fftuti[np.where(frequti>=0)]

#calculamos la magnitud del espectro
senial_fftuti_mod=np.abs(senial_fftuti)/Nuti # Respetando la relación de Parceval
# Al haberse descartado la mitad del espectro, para conservar la energía 
# original de la señal, se debe multiplicar la mitad restante por dos (excepto
# en 0 y fm/2)
senial_fftuti_mod[1:len(senial_fftuti_mod-1)] = 2 * senial_fftuti_mod[1:len(senial_fftuti_mod-1)]

#Graficación
# Se crea una gráfica que contendrá dos sub-gráficos ordenados en una fila y dos columnas
figuti, axuti = plt.subplots(1, 2, figsize=(20, 10))
figuti.suptitle('Sonido UTI', fontsize=18)

# Se grafica la señal temporal
axuti[0].plot(tuti, senialuti)
axuti[0].set_xlabel('Tiempo [s]', fontsize=15)
axuti[0].set_ylabel('Tensión [V]', fontsize=15)
axuti[0].set_title('Señal temporal', fontsize=15)
axuti[0].set_xlim([0, tsuti*Nuti])
axuti[0].grid()

# se grafica la magnitud de la respuesta en frecuencia
axuti[1].plot(futi, senial_fftuti_mod)
axuti[1].set_xlabel('Frecuencia [Hz]', fontsize=15)
axuti[1].set_ylabel('Magnitud [V]', fontsize=15)
axuti[1].set_title('Magnitud de la Respuesta en Frecuencia', fontsize=15)
axuti[1].set_xlim([0, fsuti/2])
axuti[1].grid()

plt.show()

#%% Archivo Alarmas
#Se carga el archivo
filename ='Alarmas.wav' #Nombre del archivo
fsalarma, alarma=wavfile.read(filename) #Frecuencia de muestreo y datos de señal

#Definición de parámetros temporales

tsalarma=1/fsalarma #Tiempo de muestreo
Nalarma=len(alarma) #Numero de muestras
talarma=np.linspace(0, Nalarma * tsalarma , Nalarma) #Vector de tiempos
senialalarma= alarma [:, 1] #Extraemos solo una pista del audio porque es estéreo
senialalarma= senialalarma* 3.3 / (2 ** 16 - 1) #Escalamos la señal a voltios suponiendo que se obtuvo con un CAD de 16bits y Vref 3.3V

#Cálculo de transformada de fourier

freqalarma= fftfreq(Nalarma, d=1/fsalarma) #generación del espectro de frecuencias
senial_fftalarma= fft.fft(senialalarma) #se calcula la Tansformada Rápida de Fourier

#Al ser simétrico el espectro nos quedamos solo con la parte positiva
falarma=freqalarma[np.where(freqalarma>=0)]
senial_fftalarma=senial_fftalarma[np.where(freqalarma>=0)]

#calculamos la magnitud del espectro
senial_fftalarma_mod=np.abs(senial_fftalarma)/Nalarma # Respetando la relación de Parceval
# Al haberse descartado la mitad del espectro, para conservar la energía 
# original de la señal, se debe multiplicar la mitad restante por dos (excepto
# en 0 y fm/2)
senial_fftalarma_mod[1:len(senial_fftalarma_mod-1)] = 2 * senial_fftalarma_mod[1:len(senial_fftalarma_mod-1)]

#Graficación
# Se crea una gráfica que contendrá dos sub-gráficos ordenados en una fila y dos columnas
figalarma, axalarma = plt.subplots(1, 2, figsize=(20, 10))
figalarma.suptitle('Sonido Alarmas', fontsize=18)

# Se grafica la señal temporal
axalarma[0].plot(talarma, senialalarma)
axalarma[0].set_xlabel('Tiempo [s]', fontsize=15)
axalarma[0].set_ylabel('Tensión [V]', fontsize=15)
axalarma[0].set_title('Señal temporal', fontsize=15)
axalarma[0].set_xlim([0, tsalarma*Nalarma])
axalarma[0].grid()

# se grafica la magnitud de la respuesta en frecuencia
axalarma[1].plot(falarma, senial_fftalarma_mod)
axalarma[1].set_xlabel('Frecuencia [Hz]', fontsize=15)
axalarma[1].set_ylabel('Magnitud [V]', fontsize=15)
axalarma[1].set_title('Magnitud de la Respuesta en Frecuencia', fontsize=15)
axalarma[1].set_xlim([0, fsalarma/2])
axalarma[1].grid()

plt.show()

#%% Archivo Alarmas UTI
#Se carga el archivo
filename ='Alarmas_UTI.wav' #Nombre del archivo
fsalarma_uti, alarma_uti=wavfile.read(filename) #Frecuencia de muestreo y datos de señal

#Definición de parámetros temporales

tsalarma_uti=1/fsalarma_uti #Tiempo de muestreo
Nalarma_uti=len(alarma_uti) #Numero de muestras
talarma_uti=np.linspace(0, Nalarma_uti * tsalarma_uti , Nalarma_uti) #Vector de tiempos
senialalarma_uti= alarma_uti [:, 1] #Extraemos solo una pista del audio porque es estéreo
senialalarma_uti= senialalarma_uti* 3.3 / (2 ** 16 - 1) #Escalamos la señal a voltios suponiendo que se obtuvo con un CAD de 16bits y Vref 3.3V

#Cálculo de transformada de fourier

freqalarma_uti= fftfreq(Nalarma_uti, d=1/fsalarma_uti) #generación del espectro de frecuencias
senial_fftalarma_uti= fft.fft(senialalarma_uti) #se calcula la Tansformada Rápida de Fourier

#Al ser simétrico el espectro nos quedamos solo con la parte positiva
falarma_uti=freqalarma_uti[np.where(freqalarma_uti>=0)]
senial_fftalarma_uti=senial_fftalarma_uti[np.where(freqalarma_uti>=0)]

#calculamos la magnitud del espectro
senial_fftalarma_uti_mod=np.abs(senial_fftalarma_uti)/Nalarma_uti # Respetando la relación de Parceval
# Al haberse descartado la mitad del espectro, para conservar la energía 
# original de la señal, se debe multiplicar la mitad restante por dos (excepto
# en 0 y fm/2)
senial_fftalarma_uti_mod[1:len(senial_fftalarma_uti_mod-1)] = 2 * senial_fftalarma_uti_mod[1:len(senial_fftalarma_uti_mod-1)]

#Graficación
# Se crea una gráfica que contendrá dos sub-gráficos ordenados en una fila y dos columnas
figalarma_uti, axalarma_uti = plt.subplots(1, 2, figsize=(20, 10))
figalarma_uti.suptitle('Sonido Alarmas y UTI', fontsize=18)

# Se grafica la señal temporal
axalarma_uti[0].plot(talarma_uti, senialalarma_uti)
axalarma_uti[0].set_xlabel('Tiempo [s]', fontsize=15)
axalarma_uti[0].set_ylabel('Tensión [V]', fontsize=15)
axalarma_uti[0].set_title('Señal temporal', fontsize=15)
axalarma_uti[0].set_xlim([0, tsalarma_uti*Nalarma_uti])
axalarma_uti[0].grid()

# se grafica la magnitud de la respuesta en frecuencia
axalarma_uti[1].plot(falarma_uti, senial_fftalarma_uti_mod)
axalarma_uti[1].set_xlabel('Frecuencia [Hz]', fontsize=15)
axalarma_uti[1].set_ylabel('Magnitud [V]', fontsize=15)
axalarma_uti[1].set_title('Magnitud de la Respuesta en Frecuencia', fontsize=15)
axalarma_uti[1].set_xlim([0, fsalarma_uti/2])
axalarma_uti[1].grid()

plt.show()

#%% Archivo Dräger Carina 

#Se carga el archivo
filename ='Dragger_Carina.wav' #Nombre del archivo
fscarina, carina=wavfile.read(filename) #Frecuencia de muestreo y datos de señal

#Definición de parámetros temporales

tscarina=1/fscarina #Tiempo de muestreo
Ncarina=len(carina) #Numero de muestras
tcarina=np.linspace(0, Ncarina * tscarina , Ncarina) #Vector de tiempos
senialcarina= carina[:, 1] #Extraemos solo una pista del audio porque es estéreo
senialcarina= senialcarina* 3.3 / (2 ** 16 - 1) #Escalamos la señal a voltios suponiendo que se obtuvo con un CAD de 16bits y Vref 3.3V

#Cálculo de transformada de fourier

freqcarina= fftfreq(Ncarina, d=1/fscarina) #generación del espectro de frecuencias
senial_fftcarina= fft.fft(senialcarina) #se calcula la Tansformada Rápida de Fourier

#Al ser simétrico el espectro nos quedamos solo con la parte positiva
fcarina=freqcarina[np.where(freqcarina>=0)]
senial_fftcarina=senial_fftcarina[np.where(freqcarina>=0)]

#calculamos la magnitud del espectro
senial_fftcarina_mod=np.abs(senial_fftcarina)/Ncarina # Respetando la relación de Parceval
# Al haberse descartado la mitad del espectro, para conservar la energía 
# original de la señal, se debe multiplicar la mitad restante por dos (excepto
# en 0 y fm/2)
senial_fftcarina_mod[1:len(senial_fftcarina_mod-1)] = 2 * senial_fftcarina_mod[1:len(senial_fftcarina_mod-1)]

#Graficación
# Se crea una gráfica que contendrá dos sub-gráficos ordenados en una fila y dos columnas
figcarina, axcarina = plt.subplots(1, 2, figsize=(20, 10))
figcarina.suptitle('Alarma de Dräger Carina', fontsize=18)

# Se grafica la señal temporal
axcarina[0].plot(tcarina, senialcarina)
axcarina[0].set_xlabel('Tiempo [s]', fontsize=15)
axcarina[0].set_ylabel('Tensión [V]', fontsize=15)
axcarina[0].set_title('Señal temporal', fontsize=15)
axcarina[0].set_xlim([0, tscarina*Ncarina])
axcarina[0].grid()

# se grafica la magnitud de la respuesta en frecuencia
axcarina[1].plot(fcarina, senial_fftcarina_mod)
axcarina[1].set_xlabel('Frecuencia [Hz]', fontsize=15)
axcarina[1].set_ylabel('Magnitud [V]', fontsize=15)
axcarina[1].set_title('Magnitud de la Respuesta en Frecuencia', fontsize=15)
axcarina[1].set_xlim([0, fscarina/2])
axcarina[1].grid()

plt.show()

#%% Archivo Dräger Carina con sonidos de UTI

#Se carga el archivo
filename ='Dragger_Carina_UTI.wav' #Nombre del archivo
fscarina_uti, carina_uti=wavfile.read(filename) #Frecuencia de muestreo y datos de señal

#Definición de parámetros temporales

tscarina_uti=1/fscarina_uti #Tiempo de muestreo
Ncarina_uti=len(carina_uti) #Numero de muestras
tcarina_uti=np.linspace(0, Ncarina_uti * tscarina_uti , Ncarina_uti) #Vector de tiempos
senialcarina_uti= carina_uti[:, 1] #Extraemos solo una pista del audio porque es estéreo
senialcarina_uti= senialcarina_uti* 3.3 / (2 ** 16 - 1) #Escalamos la señal a voltios suponiendo que se obtuvo con un CAD de 16bits y Vref 3.3V

#Cálculo de transformada de fourier

freqcarina_uti= fftfreq(Ncarina_uti, d=1/fscarina_uti) #generación del espectro de frecuencias
senial_fftcarina_uti= fft.fft(senialcarina_uti) #se calcula la Tansformada Rápida de Fourier

#Al ser simétrico el espectro nos quedamos solo con la parte positiva
fcarina_uti=freqcarina_uti[np.where(freqcarina_uti>=0)]
senial_fftcarina_uti=senial_fftcarina_uti[np.where(freqcarina_uti>=0)]

#calculamos la magnitud del espectro
senial_fftcarina_uti_mod=np.abs(senial_fftcarina_uti)/Ncarina_uti # Respetando la relación de Parceval
# Al haberse descartado la mitad del espectro, para conservar la energía 
# original de la señal, se debe multiplicar la mitad restante por dos (excepto
# en 0 y fm/2)
senial_fftcarina_uti_mod[1:len(senial_fftcarina_uti_mod-1)] = 2 * senial_fftcarina_uti_mod[1:len(senial_fftcarina_uti_mod-1)]

#Graficación
# Se crea una gráfica que contendrá dos sub-gráficos ordenados en una fila y dos columnas
figcarina_uti, axcarina_uti = plt.subplots(1, 2, figsize=(20, 10))
figcarina_uti.suptitle('Alarma de Dräger Carina con ruido de UTI', fontsize=18)

# Se grafica la señal temporal
axcarina_uti[0].plot(tcarina_uti, senialcarina_uti)
axcarina_uti[0].set_xlabel('Tiempo [s]', fontsize=15)
axcarina_uti[0].set_ylabel('Tensión [V]', fontsize=15)
axcarina_uti[0].set_title('Señal temporal', fontsize=15)
axcarina_uti[0].set_xlim([0, tscarina_uti*Ncarina_uti])
axcarina_uti[0].grid()

# se grafica la magnitud de la respuesta en frecuencia
axcarina_uti[1].plot(fcarina_uti, senial_fftcarina_uti_mod)
axcarina_uti[1].set_xlabel('Frecuencia [Hz]', fontsize=15)
axcarina_uti[1].set_ylabel('Magnitud [V]', fontsize=15)
axcarina_uti[1].set_title('Magnitud de la Respuesta en Frecuencia', fontsize=15)
axcarina_uti[1].set_xlim([0, fscarina_uti/2])
axcarina_uti[1].grid()

plt.show()

#%% Archivo Newport_HT50

#Se carga el archivo
filename ='Newport_HT50.wav' #Nombre del archivo
fsnewport, newport=wavfile.read(filename) #Frecuencia de muestreo y datos de señal

#Definición de parámetros temporales

tsnewport=1/fsnewport #Tiempo de muestreo
Nnewport=len(newport) #Numero de muestras
tnewport=np.linspace(0, Nnewport * tsnewport , Nnewport) #Vector de tiempos
senialnewport=newport [:, 1] #Extraemos solo una pista del audio porque es estéreo
senialnewport= senialnewport* 3.3 / (2 ** 16 - 1) #Escalamos la señal a voltios suponiendo que se obtuvo con un CAD de 16bits y Vref 3.3V

#Cálculo de transformada de fourier

freqnewport= fftfreq(Nnewport, d=1/fsnewport) #generación del espectro de frecuencias
senial_fftnewport= fft.fft(senialnewport) #se calcula la Tansformada Rápida de Fourier

#Al ser simétrico el espectro nos quedamos solo con la parte positiva
fnewport=freqnewport[np.where(freqnewport>=0)]
senial_fftnewport=senial_fftnewport[np.where(freqnewport>=0)]

#calculamos la magnitud del espectro
senial_fftnewport_mod=np.abs(senial_fftnewport)/Nnewport # Respetando la relación de Parceval
# Al haberse descartado la mitad del espectro, para conservar la energía 
# original de la señal, se debe multiplicar la mitad restante por dos (excepto
# en 0 y fm/2)
senial_fftnewport_mod[1:len(senial_fftnewport_mod-1)] = 2 * senial_fftnewport_mod[1:len(senial_fftnewport_mod-1)]

#Graficación
# Se crea una gráfica que contendrá dos sub-gráficos ordenados en una fila y dos columnas
fignewport, axnewport = plt.subplots(1, 2, figsize=(20, 10))
fignewport.suptitle('Alarma de Newport HT50 ', fontsize=18)

# Se grafica la señal temporal
axnewport[0].plot(tnewport, senialnewport)
axnewport[0].set_xlabel('Tiempo [s]', fontsize=15)
axnewport[0].set_ylabel('Tensión [V]', fontsize=15)
axnewport[0].set_title('Señal temporal', fontsize=15)
axnewport[0].set_xlim([0, tsnewport*Nnewport])
axnewport[0].grid()

# se grafica la magnitud de la respuesta en frecuencia
axnewport[1].plot(fnewport, senial_fftnewport_mod)
axnewport[1].set_xlabel('Frecuencia [Hz]', fontsize=15)
axnewport[1].set_ylabel('Magnitud [V]', fontsize=15)
axnewport[1].set_title('Magnitud de la Respuesta en Frecuencia', fontsize=15)
axnewport[1].set_xlim([0, fsnewport/2])
axnewport[1].grid()

plt.show()

#%% Archivo Newport_HT50 con ruido de UTI

#Se carga el archivo
filename ='Newport_HT50_UTI.wav' #Nombre del archivo
fsnewport_uti, newport_uti=wavfile.read(filename) #Frecuencia de muestreo y datos de señal

#Definición de parámetros temporales

tsnewport_uti=1/fsnewport_uti #Tiempo de muestreo
Nnewport_uti=len(newport_uti) #Numero de muestras
tnewport_uti=np.linspace(0, Nnewport_uti * tsnewport_uti , Nnewport_uti) #Vector de tiempos
senialnewport_uti=newport_uti [:, 1] #Extraemos solo una pista del audio porque es estéreo
senialnewport_uti= senialnewport_uti* 3.3 / (2 ** 16 - 1) #Escalamos la señal a voltios suponiendo que se obtuvo con un CAD de 16bits y Vred 3.3v

#Cálculo de transformada de fourier

freqnewport_uti= fftfreq(Nnewport_uti, d=1/fsnewport_uti) #generación del espectro de frecuencias
senial_fftnewport_uti= fft.fft(senialnewport_uti) #se calcula la Tansformada Rápida de Fourier

#Al ser simétrico el espectro nos quedamos solo con la parte positiva
fnewport_uti=freqnewport_uti[np.where(freqnewport_uti>=0)]
senial_fftnewport_uti=senial_fftnewport_uti[np.where(freqnewport_uti>=0)]

#calculamos la magnitud del espectro
senial_fftnewport_uti_mod=np.abs(senial_fftnewport_uti)/Nnewport_uti # Respetando la relación de Parceval
# Al haberse descartado la mitad del espectro, para conservar la energía 
# original de la señal, se debe multiplicar la mitad restante por dos (excepto
# en 0 y fm/2)
senial_fftnewport_uti_mod[1:len(senial_fftnewport_uti_mod-1)] = 2 * senial_fftnewport_uti_mod[1:len(senial_fftnewport_uti_mod-1)]

#Graficación
# Se crea una gráfica que contendrá dos sub-gráficos ordenados en una fila y dos columnas
fignewport_uti, axnewport_uti = plt.subplots(1, 2, figsize=(20, 10))
fignewport_uti.suptitle('Alarma de Newport HT50 con ruido de UTI', fontsize=18)

# Se grafica la señal temporal
axnewport_uti[0].plot(tnewport_uti, senialnewport_uti)
axnewport_uti[0].set_xlabel('Tiempo [s]', fontsize=15)
axnewport_uti[0].set_ylabel('Tensión [V]', fontsize=15)
axnewport_uti[0].set_title('Señal temporal', fontsize=15)
axnewport_uti[0].set_xlim([0, tsnewport_uti*Nnewport_uti])
axnewport_uti[0].grid()

# se grafica la magnitud de la respuesta en frecuencia
axnewport_uti[1].plot(fnewport_uti, senial_fftnewport_uti_mod)
axnewport_uti[1].set_xlabel('Frecuencia [Hz]', fontsize=15)
axnewport_uti[1].set_ylabel('Magnitud [V]', fontsize=15)
axnewport_uti[1].set_title('Magnitud de la Respuesta en Frecuencia', fontsize=15)
axnewport_uti[1].set_xlim([0, fsnewport_uti/2])
axnewport_uti[1].grid()

plt.show()

#%% Archivo Puritan Bennett 840
filename ='Puritan_Bennett_840.wav' #Nombre del archivo
fspuritan, puritan=wavfile.read(filename) #Frecuencia de muestreo y datos de señal

#Definición de parámetros temporales

tspuritan=1/fspuritan #Tiempo de muestreo
Npuritan=len(puritan) #Numero de muestras
tpuritan=np.linspace(0, Npuritan * tspuritan , Npuritan) #Vector de tiempos
senialpuritan=puritan [:, 1] #Extraemos solo una pista del audio porque es estéreo
senialpuritan= senialpuritan* 3.3 / (2 ** 16 - 1) #Escalamos la señal a voltios suponiendo que se obtuvo con un CAD de 16bits y Vred 3.3v

#Cálculo de transformada de fourier

freqpuritan= fftfreq(Npuritan, d=1/fspuritan) #generación del espectro de frecuencias
senial_fftpuritan= fft.fft(senialpuritan) #se calcula la Tansformada Rápida de Fourier

#Al ser simétrico el espectro nos quedamos solo con la parte positiva
fpuritan=freqpuritan[np.where(freqpuritan>=0)]
senial_fftpuritan=senial_fftpuritan[np.where(freqpuritan>=0)]

#calculamos la magnitud del espectro
senial_fftpuritan_mod=np.abs(senial_fftpuritan)/Npuritan # Respetando la relación de Parceval
# Al haberse descartado la mitad del espectro, para conservar la energía 
# original de la señal, se debe multiplicar la mitad restante por dos (excepto
# en 0 y fm/2)
senial_fftpuritan_mod[1:len(senial_fftpuritan_mod-1)] = 2 * senial_fftpuritan_mod[1:len(senial_fftpuritan_mod-1)]

#Graficación
# Se crea una gráfica que contendrá dos sub-gráficos ordenados en una fila y dos columnas
figpuritan, axpuritan = plt.subplots(1, 2, figsize=(20, 10))
figpuritan.suptitle('Alarma de Puritan Bennett 840', fontsize=18)

# Se grafica la señal temporal
axpuritan[0].plot(tpuritan, senialpuritan)
axpuritan[0].set_xlabel('Tiempo [s]', fontsize=15)
axpuritan[0].set_ylabel('Tensión [V]', fontsize=15)
axpuritan[0].set_title('Señal temporal', fontsize=15)
axpuritan[0].set_xlim([0, tspuritan*Npuritan])
axpuritan[0].grid()

# se grafica la magnitud de la respuesta en frecuencia
axpuritan[1].plot(fpuritan, senial_fftpuritan_mod)
axpuritan[1].set_xlabel('Frecuencia [Hz]', fontsize=15)
axpuritan[1].set_ylabel('Magnitud [V]', fontsize=15)
axpuritan[1].set_title('Magnitud de la Respuesta en Frecuencia', fontsize=15)
axpuritan[1].set_xlim([0, fspuritan/2])
axpuritan[1].grid()

plt.show()

#%% Audio Puritan Bennett 840 con ruido de UTI

filename ='Puritan_Bennett_840_UTI.wav' #Nombre del archivo
fspuritan_uti, puritan_uti=wavfile.read(filename) #Frecuencia de muestreo y datos de señal

#Definición de parámetros temporales

tspuritan_uti=1/fspuritan_uti #Tiempo de muestreo
Npuritan_uti=len(puritan_uti) #Numero de muestras
tpuritan_uti=np.linspace(0, Npuritan_uti * tspuritan_uti , Npuritan_uti) #Vector de tiempos
senialpuritan_uti=puritan_uti [:, 1] #Extraemos solo una pista del audio porque es estéreo
senialpuritan_uti= senialpuritan_uti* 3.3 / (2 ** 16 - 1) #Escalamos la señal a voltios suponiendo que se obtuvo con un CAD de 16bits y Vred 3.3v

#Cálculo de transformada de fourier

freqpuritan_uti= fftfreq(Npuritan_uti, d=1/fspuritan_uti) #generación del espectro de frecuencias
senial_fftpuritan_uti= fft.fft(senialpuritan_uti) #se calcula la Tansformada Rápida de Fourier

#Al ser simétrico el espectro nos quedamos solo con la parte positiva
fpuritan_uti=freqpuritan_uti[np.where(freqpuritan_uti>=0)]
senial_fftpuritan_uti=senial_fftpuritan_uti[np.where(freqpuritan_uti>=0)]

#calculamos la magnitud del espectro
senial_fftpuritan_uti_mod=np.abs(senial_fftpuritan_uti)/Npuritan_uti # Respetando la relación de Parceval
# Al haberse descartado la mitad del espectro, para conservar la energía 
# original de la señal, se debe multiplicar la mitad restante por dos (excepto
# en 0 y fm/2)
senial_fftpuritan_uti_mod[1:len(senial_fftpuritan_uti_mod-1)] = 2 * senial_fftpuritan_uti_mod[1:len(senial_fftpuritan_uti_mod-1)]

#Graficación
# Se crea una gráfica que contendrá dos sub-gráficos ordenados en una fila y dos columnas
figpuritan_uti, axpuritan_uti = plt.subplots(1, 2, figsize=(20, 10))
figpuritan_uti.suptitle('Alarma de Puritan Bennett 840 con ruido de UTI', fontsize=18)

# Se grafica la señal temporal
axpuritan_uti[0].plot(tpuritan_uti, senialpuritan_uti)
axpuritan_uti[0].set_xlabel('Tiempo [s]', fontsize=15)
axpuritan_uti[0].set_ylabel('Tensión [V]', fontsize=15)
axpuritan_uti[0].set_title('Señal temporal', fontsize=15)
axpuritan_uti[0].set_xlim([0, tspuritan_uti*Npuritan_uti])
axpuritan_uti[0].grid()

# se grafica la magnitud de la respuesta en frecuencia
axpuritan_uti[1].plot(fpuritan_uti, senial_fftpuritan_uti_mod)
axpuritan_uti[1].set_xlabel('Frecuencia [Hz]', fontsize=15)
axpuritan_uti[1].set_ylabel('Magnitud [V]', fontsize=15)
axpuritan_uti[1].set_title('Magnitud de la Respuesta en Frecuencia', fontsize=15)
axpuritan_uti[1].set_xlim([0, fspuritan_uti/2])
axpuritan_uti[1].grid()

plt.show()
#%%