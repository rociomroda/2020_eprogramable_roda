/*! @mainpage conversor_ad
 *
 * \section genDesc General Description
 *
 *
 *Disparo de conversión AD con interrupción periódica de timer de 1/15Hz (67ms).
 *
 *Obtención de datos analógicos mediante el CH3 y se los convierte a digitales, es controlada por el timer. Cuando está lista la conversión, se sacan los valores por la UART y el graficador en serie.
 *Cada 1s se muestra el promedio de las muestras acumuladas en ese tiempo.
 *
 *
 * \section hardConn Hardware Connection
 *
 * | Potenciometro  |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	OUT 	    | 	CH3	        |
 * |    GND         |   GNDA        |
 * |    VCC         |   VDDA        |
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 02/11/2020 | Creación del documento		                 |
 *
 * @author Roda Rocío Milagros
 *
 */


#ifndef _CONVERSOR_AD_H
#define _CONVERSOR_AD_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _CONVERSOR_AD_H */

