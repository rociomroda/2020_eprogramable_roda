/*! @mainpage riego_inteligente
 *
 * @section genDesc General Description
 *
 *
 *
 *Mediante RTC de configura la fecha actual. Luego se comprueba en la estación del año.
 *
 *Sensor analógico de humedad envia datos mediante CH1. Sensor analógico de luz (LDR) envía datos mediante CH2.
 *
 *Cada un numero de muestras establecido, promedia las muestras y chequea el nivel de humedad en base al promedio.
 *
 *Si ese promedio es menor a 512(corresponde al 50% de humedad) se chequea el nivel de luz.
 *
 *Si el nivel de luz es bajo, se chequea la hora actual. Si la hora está dentro de los parámetros de regado de la estación se riega.
 *
 *Se riega por un intervalo de tiempo determinado y se vuelve a chequear los niveles de humedad. Si son menores a 307 (70% de humedad aprox) se riega otra vez.
 *
 *La escala de humedad es la siguiente: 0 corresponde a 100% de humedad y 1023 a 0%
 *
 *En el LDR cuando hay mucha luz, la resistencia el baja por ende el valor maximo está cerca de 1023. En la oscuridad es al revés. Tomamos como poca luz el dato 400.
 *
 *Tomamos ese dato de 400 debido al supuesto uso del LDR modelo GL5528.
 *
 *
 *
 *
 *
 * @section hardConn Hardware Connection
 *
 * | Sensor humedad |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	OUT 	    | 	CH1	        |
 * |    GND         |   GNDA        |
 * |    VCC         |   VDDA o 3.3V |
 *
 * | Sensor LDR     |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	OUT 	    | 	CH2	        |
 * |    GND         |   GND         |
 * |    VCC         | VDDA o 3.3V   |
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 26/10/2020 | Creacion del documento		                 |
 * | 6/11/2020 | Version final                                   |
 *
 * @author Roda Rocío Milagros
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/riego_inteligente.h"       /* <= own header */
#include "systemclock.h"
#include "bool.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"
#include "led.h"
#include "sapi_rtc.h"

/*==================[macros and definitions]=================================*/


#define FALSE 0
#define TRUE 1
#define BAUDIO 115200 /*Velocidad de transmisión por UART*/
#define POCALUZ 400 /*Umbral luz*/
#define SECO 512 /*Umbral para regar*/
#define HUMEDO 307 /* Umbral para dejar de regar*/
#define CONTADORPROMEDIO 2 /*Umbral cantidad de datos para luego hacer el promedio*/
#define ANIO 2020 /*Dato año para config RTC*/
#define DIA 25 /*Dato día para config RTC*/
#define MES 11 /*Dato mes para config RTC*/
#define SEMANA 2 /*Dato semana para config RTC*/
#define HORA 19 /*Dato hora para config RTC*/
#define MINUTOS 30 /*Dato minutos para config RTC*/
#define SEG 0 /*Dato segundo para config RTC*/
#define FIN 20 /*Dato dia fin de estaciones*/
#define INICIO 21 /*Dato dia inicio de estaciones*/
#define MESIV 12 /*Dato mes inicio de verano*/
#define MESFV 3 /*Dato mes fin de verano*/
#define MESFO 6 /*Dato mes fin de otoño*/
#define MESFI 9 /*Dato mes fin de invierno*/
#define IREGADOV 20 /*Dato cota inferior franja horaria a regar en verano y primavera*/
#define FREGADOV 4 /*Dato cota superior franja horaria a regar en verano y primavera*/
#define IREGADOO 19 /*Dato cota inferior franja horaria a regar en otoño*/
#define IREGADOI 18 /*Dato  franja horaria a regar en invierno*/
#define TREGADO 5 /* Cantidad de intervalos de timer de regado */
#define TIMERHUMEDAD 10000 /*Tiempo timer humedad*/
#define TIMERREG 1000 /*Tiempo timer regado*/
#define ANALOGMAX 1023 /*Maximo valor analógico*/
#define ROSCURIDAD 1000 /*Resistencia del LDR en oscuridad*/
#define RLUZ 15 /*Resistencia del LDR en luz*/
#define RCALIBRACION 10 /*Resistencia de calibración LDR */

bool dac=FALSE;
bool adc=FALSE;

/*Banderas para conversión DA, AC e interrupciones a teclas*/
uint16_t humedad=0; /*Creo una variable que guarde la humedad*/
uint16_t luz=0;/*Creo una variable que guarde la luz obtenida*/
uint16_t timerregado=0; /*Creo una variable que cuente los ciclos del timer regado*/
uint8_t contadorhumedad=0;/* Variables anteriores  y actuales no un vector*/
uint16_t sumatoriahumedad=0;/*Variable que va acumulando los valores de humedad*/
uint16_t promedio=0;/*Variable que guarda el promedio*/
uint16_t luminancia=0;/* Variable que guarda luminancia*/
uint16_t despuesderegado=0; /*Varaible que guarda medición de humedad luego de regar*/
/*==================[internal data definition]===============================*/

/*void LecturaUart(){

}*/
void MedirHumedad(){
	AnalogInputRead(CH1,&humedad);
	sumatoriahumedad=sumatoriahumedad+humedad;
	UartSendString(SERIAL_PORT_PC," humedad es:  ");
	UartSendString(SERIAL_PORT_PC, UartItoa(humedad, 10));
	UartSendString(SERIAL_PORT_PC," \r\n");
	contadorhumedad ++;
	adc=TRUE;
}
void FuncionamientoTimer(){/*Interrupción del timer*/
	AnalogStartConvertion();/*Inicio de conversión analógica*/
	MedirHumedad();

}
void Timerregado(){
	timerregado++;
}

void MedirLuz(){
	AnalogStartConvertion();
	AnalogInputRead(CH2,&luz);
    luminancia=(luz*ROSCURIDAD*RCALIBRACION)/(RLUZ*RCALIBRACION*(ANALOGMAX-luz));/*COnvierto los datos a lux*/
	UartSendString(SERIAL_PORT_PC, UartItoa(luminancia, 10));
	UartSendString(SERIAL_PORT_PC," luxes\r\n");
}

void Promedio(){/*Promedio */
	promedio=sumatoriahumedad/contadorhumedad;
	UartSendString(SERIAL_PORT_PC,"El promedio es: ");
	UartSendString(SERIAL_PORT_PC, UartItoa(promedio, 10));
	UartSendString(SERIAL_PORT_PC,"\r\n");

}
/*==================[macros and definitions]=================================*/

rtc_t fecha={ANIO,MES,DIA,SEMANA,HORA,MINUTOS,SEG};
analog_input_config sensor_luz ={CH2, AINPUTS_SINGLE_READ,MedirLuz};
analog_input_config sensor_humedad ={CH1, AINPUTS_SINGLE_READ,MedirHumedad};/*Configuro struct de analogico*/
timer_config medicionhumedad={TIMER_A,TIMERHUMEDAD,FuncionamientoTimer}; /*cADA MEDIO MINUTO*/
timer_config reg={TIMER_B,TIMERREG,Timerregado};
serial_config mi_uart ={SERIAL_PORT_PC, BAUDIO, NO_INT}; /*Configuro struct de uart*/

/*==================[internal data definition]===============================*/
/*Funcion de riego*/
void Regar(){
	TimerStart(reg.timer);/*Inicio timer de regado*/
	while(promedio>HUMEDO){
		if(timerregado<=TREGADO){/*Riego durante la cantidad de ciclos establecidos por TREGADO */
			LedOn(LED_1);
		}
		else{
			TimerStop(reg.timer);/*detengo timer de regado y lo reseteo*/
			TimerReset(reg.timer);
			timerregado=0; /*Llevo a 0 el contador de ciclos del timer*/
			LedOff(LED_1);
			AnalogStartConvertion();
			AnalogInputRead(CH1,&despuesderegado);
			promedio=despuesderegado; /*Vuelvo a tomar humedad y la igualo al promedio para volver al while ciclo while*/
		}

	}



}


void Verano(){
	while (contadorhumedad<=CONTADORPROMEDIO){
		if (adc ==TRUE){
				if(contadorhumedad==CONTADORPROMEDIO){
					Promedio();
					if(promedio>SECO){
						MedirLuz();
						if(luz<POCALUZ){
							rtcRead(&fecha);

							if((fecha.hour>=IREGADOO)|| (fecha.hour<FREGADOV)){
								Regar();

							}/*FIN IF REGADO*/


						}/*FIN IF CHEQUEO LUZ*/
					}/*FIN IF CHEQUEO HUMEDAD*/

				}/*FIN IF PROMEDIO*/
				adc=FALSE;

			}/*FIN IF ADC*/


	}


}/*FIN VERANO*/

void Otono(){
	while (contadorhumedad<=CONTADORPROMEDIO){
		if (adc ==TRUE){
				if(contadorhumedad==CONTADORPROMEDIO){
					Promedio();

					if(promedio>SECO){
						MedirLuz();

						if(luz<POCALUZ){
							rtcRead(&fecha);

							if((fecha.hour>=IREGADOV)|| (fecha.hour<FREGADOV)){
								Regar();

							}/*FIN IF REGADO*/


						}/*FIN IF CHEQUEO LUZ*/

					}/*FIN IF CHEQUEO HUMEDAD*/
				}/*FIN IF PROMEDIO*/
				adc=FALSE;

			}/*FIN IF ADC*/


	}


}

void Invierno(){
	while (contadorhumedad<=CONTADORPROMEDIO){
		if (adc ==TRUE){
				if(contadorhumedad==CONTADORPROMEDIO){
					Promedio();
					if(promedio>SECO){
						MedirLuz();
						if(luz<POCALUZ){
							rtcRead(&fecha);

							if((fecha.hour>=IREGADOI)){
								Regar();

							}/*FIN IF REGADO*/


						}/*FIN IF CHEQUEO LUZ*/

					}/*FIN IF CHEQUEO HUMEDAD*/

				}/*FIN IF PROMEDIO*/
				adc=FALSE;

			}/*FIN IF ADC*/



	}

}

void Primavera(){
	while (contadorhumedad<=CONTADORPROMEDIO){
		if (adc ==TRUE){
				if(contadorhumedad==CONTADORPROMEDIO){
					Promedio();

					if(promedio>SECO){
						MedirLuz();
						if(luz<POCALUZ){
							rtcRead(&fecha);

							if((fecha.hour>=IREGADOV)|| (fecha.hour<FREGADOV)){
								Regar();

							}/*FIN IF REGADO*/


						}/*FIN IF CHEQUEO LUZ*/

					}/*FIN IF CHEQUEO HUMEDAD*/


				}/*FIN IF PROMEDIO*/
				adc=FALSE;

			}/*FIN IF ADC*/


	}

}/*FIN PRIMAVERA*/




/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void){

	SystemClockInit();/*Inicializo clock del CPU*/
	LedsInit(); /*Inicializo leds*/
	SwitchesInit();/*Inicializo las teclas*/

	rtcConfig (&fecha);/*Configuro RTC*/
	TimerInit(&reg);/*Inicializo timer regado*/
	TimerInit(&medicionhumedad);/*Inicializo timer para medición de humedad*/


	UartInit(&mi_uart); /*Inicializo Uart*/

	AnalogInputInit(&sensor_humedad);/*Inicializo entrada del sensor de humedad*/
	AnalogOutputInit();/*Inicializo output*/
	AnalogInputInit(&sensor_luz);/*Inicializo entrada del sensor de luz*/
	TimerStart(medicionhumedad.timer);/*Inicializo timer del sensor de humedad*/


while (1) {
	rtcRead(&fecha); /*Verifico estaciones, prendo leds para indicar estación que fue elegida*/
	if(fecha.month<MESFV){
		LedOn(LED_RGB_R);/*Prende el color rojo el led RGB*/
		Verano();
		LedOff(LED_RGB_R);
	}
	if(((fecha.month==MESIV)&&(fecha.mday>=INICIO)) || ((fecha.month==MESFV)&&(fecha.mday<=FIN))){
		LedOn(LED_RGB_R);/*Prende el color rojo el led RGB*/
		Verano();
		LedOff(LED_RGB_R);

	}
	if((fecha.month>MESFV)&&(fecha.month<MESFO)){
		LedOn(LED_2);/*Prende el LED 2 de la placa*/
		Otono();
		LedOff(LED_2);
	}
	if(((fecha.month==MESFO)&&(fecha.mday<=FIN)) || ((fecha.month==MESFV)&&(fecha.mday>=INICIO))){
		LedOn(LED_2);/*Prende el LED 2 de la placa*/
		Otono();
		LedOff(LED_2);

	}
	if((fecha.month<MESFI)&&(fecha.month>MESFO)){
		LedOn(LED_RGB_B);/*Prende el color azul el led RGB*/
		Invierno();
		LedOff(LED_RGB_B);
	}
	if(((fecha.month==MESFI)&&(fecha.mday<=FIN)) || ((fecha.month==MESFO)&&(fecha.mday>=INICIO))){
		LedOn(LED_RGB_B);/*Prende el color azul el led RGB*/
		Invierno();
		LedOff(LED_RGB_B);
	}
	if((fecha.month>MESFI)&&(fecha.month<MESIV)){
		LedOn(LED_RGB_G);/*Prende el color verde el led RGB*/
		Primavera();
		LedOff(LED_RGB_G);
	}
	if(((fecha.month==MESIV)&&(fecha.mday<=FIN)) || ((fecha.month==MESFI)&&(fecha.mday>=INICIO))){
		LedOn(LED_RGB_G);/*Prende el color verde el led RGB*/
		Primavera();
		LedOff(LED_RGB_G);

	}
	promedio=0;
    sumatoriahumedad=0;
	contadorhumedad=0;



}/*FIN DEL WHILE*/
}/*FIN DEL MAIN*/

/*==================[end of file]============================================*/

