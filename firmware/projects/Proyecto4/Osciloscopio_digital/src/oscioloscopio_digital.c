/*! @mainpage osciloscopio_digital
 *
 * \section genDesc General Description
 *
 *Video de funcionamiento: https://drive.google.com/file/d/12e_fI-Rxz8hnKlvCvmPGl2zNcNFM1nbk/view?usp=sharing
 *
 *Disparo de conversión AD con interrupción periódica de timer de 1/500Hz (2ms).
 *
 *Obtención de datos analógicos mediante el CH1 y se los convierte a digitales, es controlada por el timer. Cuando está lista la conversión, se sacan los valores por la UART y el graficador en serie.
 *
 *Muestreo de señal de ECG por la UART y graficador en serie que previamente es convertida de digital a analógica.
 *
 *La misma señal de ECG es mostrada 2 veces en el graficador pero si se apreta la TECLA 1, a una de ellas se le aplica un filtro pasabajos de 20Hz de frecuencia.
 *Si se aprieta la TECLA2, se deshabilita el filtro, si se aprieta TECLA 3 baja la frecuencia de corte en 5 unidades, la cota de la misma es de 20 Hz.
 *Si se aprieta la TECLA4, aumenta la frecuencia de corte en 5 unidades. La cota superior de frecuencia de corte es de 60Hz.
 *
 *
 * \section hardConn Hardware Connection
 *
 * | Potenciometro  |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	OUT 	    | 	CH1	        |
 * |    GND         |   GNDA        |
 * |    VCC         |   DAC         |
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 10/10/2020 | Creacion del documento		                 |
 * | 10/10/2020 | Funcionalidades de uart y timer                |
 * | 15/10/2020 | Funcionalidades DAC                            |
 * | 23/10/2020 | Version final, funcionalidades ADC agregadas   |
 *
 * @author Roda Rocío Milagros
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/osciloscopio_digital.h"       /* <= own header */
#include "systemclock.h"
#include "bool.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"
#include "led.h"


/*==================[macros and definitions]=================================*/

#define COUNT_DELAY 3000000
#define FALSE 0
#define TRUE 1
#define BAUDIO 115200 /*Velocidad de transmisión por UART*/
#define BUFFER_SIZE 231 /*Tamaño de ECG*/

bool dac=FALSE;
bool tecla1=FALSE;
bool tecla3=FALSE;
bool tecla4=FALSE;
bool adc=FALSE;
/*Banderas para conversión DA, AC e interrupciones a teclas*/
uint16_t valor; /*Creo una variable que guarde el valor convertido*/
float alfa=0;
uint8_t fc=20;
float rc=0;
float dt=0;
/*alfa, fc (frecuencia de corte), rc(R*C) y dt son términos utilizados en el filtrado*/
const uint8_t SALTO= 5;/*constante del salto de frec*/
const uint8_t MIN= 5;/*cota inferior frecuencia de corte*/
const uint8_t MAX=60;/*cota superior de frecuencia de corte*/


const char ecg[BUFFER_SIZE]={
		76, 77, 78, 77, 79, 86, 81, 76, 84, 93, 85, 80,
		89, 95, 89, 85, 93, 98, 94, 88, 98, 105, 96, 91,
		99, 105, 101, 96, 102, 106, 101, 96, 100, 107, 101,
		94, 100, 104, 100, 91, 99, 103, 98, 91, 96, 105, 95,
		88, 95, 100, 94, 85, 93, 99, 92, 84, 91, 96, 87, 80,
		83, 92, 86, 78, 84, 89, 79, 73, 81, 83, 78, 70, 80, 82,
		79, 69, 80, 82, 81, 70, 75, 81, 77, 74, 79, 83, 82, 72,
		80, 87, 79, 76, 85, 95, 87, 81, 88, 93, 88, 84, 87, 94,
		86, 82, 85, 94, 85, 82, 85, 95, 86, 83, 92, 99, 91, 88,
		94, 98, 95, 90, 97, 105, 104, 94, 98, 114, 117, 124, 144,
		180, 210, 236, 253, 227, 171, 99, 49, 34, 29, 43, 69, 89,
		89, 90, 98, 107, 104, 98, 104, 110, 102, 98, 103, 111, 101,
		94, 103, 108, 102, 95, 97, 106, 100, 92, 101, 103, 100, 94, 98,
		103, 96, 90, 98, 103, 97, 90, 99, 104, 95, 90, 99, 104, 100, 93,
		100, 106, 101, 93, 101, 105, 103, 96, 105, 112, 105, 99, 103, 108,
		99, 96, 102, 106, 99, 90, 92, 100, 87, 80, 82, 88, 77, 69, 75, 79,
		74, 67, 71, 78, 72, 67, 73, 81, 77, 71, 75, 84, 79, 77, 77, 76, 76,
};
float salida_filtrada=0;/* Variables anteriores  y actuales no un vector*/
float salida_filtradaant=0;
const float PHI=3.141;
/*==================[internal data definition]===============================*/
void Delay(){
	uint32_t i;

	for(i=COUNT_DELAY; i!=0; i--)
	{
		   asm  ("nop");
	}
}

/*void LecturaUart(){

}*/
void Minimo(){/*Función que disminuye la frecuencia de a 5 unidades*/
 if (fc > MIN) { /*Si la frecuencia es mayor que la de corte inf establecida, entra acá, sino pasa al ELSE*/
	fc = fc - SALTO;
	rc=1/(2*PHI*fc);
	alfa=dt/(dt+rc);
			}/*FIN DEL IF ALFA*/
			else {
				fc = fc;
			}
}
void Maximo(){/*Función que sube 1 unidad la frecuencia*/

	if (fc < MAX) {/*Si la frecuencia es menor que 60 (max establecida de corte, entra en el if*/
			fc = fc + SALTO;
			rc=1/(2*PHI*fc);
			alfa=dt/(dt+rc);
		}/*Cálculo con la nueva frecuencia*/
	else {
		fc = fc;
	}
}
void Tecla1(){
	tecla1=TRUE;/*Cambia el estado de la bandera tecla1*/
}
void Tecla2(){
	tecla1=FALSE;/*Cambia el estado de la bandera tecla2*/
	LedOff(LED_1);/*APAGO LED QUE PRENDE AL ESTAR ACTIVADA TECLA1*/
}

void Tecla3(){/*Interrupcion TECLA3*/
	tecla3=TRUE;/*Cambio de bandera, activada*/
	LedOn(LED_2);/*Prendo led para saber si está funcionando la TECLA3*/
}

void Tecla4(){/*Interrupción TECLA4*/
	tecla4=TRUE;/*Cambio de bandera, activada*/
	LedOn(LED_3);/*Prendo led para saber si está funcionando la TECLA4*/

}
void FuncionADC(){/*Interrupción ADC*/
	AnalogInputRead(CH1,&valor);/*Lectura de datos del CH1*/
	adc=TRUE;/*Cambio de bandera de conversión AD, activada*/


	/*UartSendString envía al puerto de la pc datos string que se veran en la consola*/

}


void FuncionamientoTimer(){/*Interrupción del timer*/
	AnalogStartConvertion();/*Inicio de conversión analógica*/

}
void TimerDAC(){/*Interrupcion conversor digital a analogico*/
	dac=TRUE;/*Bandera de conversión DA, activada*/
}


/*Las funciones Tecla1,Tecla2,Tecla3 indican lo que hace el programa de acuerdo a la interrupción que ocurre*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void){

	SystemClockInit();/*Inicializo clock del CPU*/
	LedsInit(); /*Inicializo leds*/
	SwitchesInit();/*Inicializo las teclas*/

	timer_config mi_ACD={TIMER_A,2,FuncionamientoTimer}; /*Configuro struct con timer, periodo 1/500Hz y función que quiero que ejecute*/
	TimerInit(&mi_ACD); /*Inicializo timer*/
	timer_config salida_DAC={TIMER_B,4,TimerDAC};/*Configuro struct con timer, periodo 1/231 y función que quiero que ejecute*/
	TimerInit(&salida_DAC); /*Inicializo timer*/
	TimerStart(mi_ACD.timer);/*Comienza a contar el timer*/
	TimerStart(salida_DAC.timer);/*Comienza a contar el timer*/

	serial_config mi_uart ={SERIAL_PORT_PC, BAUDIO, NO_INT}; /*Configuro struct de uart*/
	UartInit(&mi_uart);/*Inicializo Uart*/

	analog_input_config conf_analog ={CH1, AINPUTS_SINGLE_READ,FuncionADC};/*Configuro struct de analogico*/
	AnalogInputInit(&conf_analog);/*Inicializo input*/
	AnalogOutputInit();/*Inicializo output*/

	SwitchActivInt(SWITCH_1,Tecla1);
	SwitchActivInt(SWITCH_2,Tecla2);
	SwitchActivInt(SWITCH_3,Tecla3);
	SwitchActivInt(SWITCH_4,Tecla4);
	/*Inicializo teclas*/

	uint8_t i=0;
	dt=(float)mi_ACD.period/1000;/*Le doy valor a dt*/
	rc=1/(2*PHI*fc);/*Calculo RC*/
	alfa=dt/(dt+rc);/*Calculo alfa*/
while (1) {
	if (dac == TRUE) {
		if (i < BUFFER_SIZE) {

			AnalogOutputWrite(ecg[i]);/*Muestro valores del vector ECG convertido de digital a analógico*/
			i++;


		}/*fin if buffer*/
		else {
			i = 0;
		}


		dac = FALSE;
	}/*FIN DAC*/
	if (adc ==TRUE){
		if (tecla1 == TRUE) {
						LedOn(LED_1);/*Prendo led que me indica filtro activo*/
						salida_filtrada=salida_filtradaant+alfa*(valor-salida_filtradaant);/*Filtro la señal*/
						salida_filtradaant=salida_filtrada;/*guardo resultado para el proximo cálculo del filtro*/
						if (tecla3 == TRUE) {
							Minimo();
							tecla3=FALSE;
							LedOff(LED_2);/*Apago LED*/
						}/*FIN TECLA 3*/
						if (tecla4 == TRUE) {/*Aumento de Frec de muestreo*/
							Maximo();
							tecla4=FALSE;
							LedOff(LED_3);/*Apago LED*/
						}/*FIN TECLA 4*/

					}/*Fin tecla 1*/
					else{
						salida_filtrada=valor;/*Si no está activa la tecla1, la salida filtrada=ECG*/
					}
		UartSendString(SERIAL_PORT_PC, UartItoa(valor, 10));
		UartSendString(SERIAL_PORT_PC,",");
		UartSendString(SERIAL_PORT_PC, UartItoa(salida_filtrada, 10));
		UartSendString(SERIAL_PORT_PC,"\r");
		adc=FALSE;
				/*Se muestran por la UART el valor del CH1 y del ECG de la salida filtrada*/
	}/*FIN ADC*/

}/*FIN DEL WHILE*/
}/*FIN DEL MAIN*/

/*==================[end of file]============================================*/

