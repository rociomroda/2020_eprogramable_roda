/*! @mainpage Ejercicio_Complementario_Con_Interrupciones
 *
 * \section genDesc General Description
 *
 *https://youtu.be/hXC1S3V84P4
 *
 *Object counter starts when switch 1 is true.
 *Leds show the number of count in binary and they are off when the counter is zero or when switch 1 is false.
 *When switch 1 is true and switch 2 too, leds show the last count before push switch 2, but the counter continuous counting objects
 *When switch 3 is true, the counter returns to zero and starts again.
 *When switch 1 is false, all leds are off and counter isn't count.
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	OUT 	    | 	TCOL0	    |
 * |    GND         |   GND         |
 * |    VCC         |   5 V         |
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 19/09/2020 | Document creation		                         |
 * | 24/09/2020 | Last version. Functionalities were added       |
 *
 * @author Roda Rocío Milagros
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/ejercicio_complementario_con_interrupciones_.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "bool.h"
#include "Tcrt5000.h"
#include "switch.h"
/*==================[macros and definitions]=================================*/
#define OBJET 1
#define NOBJET 0
#define COUNT_DELAY 3000000
#define FALSE 0
#define TRUE 1


uint8_t contador=0; /*Creo una variable que cuente la cantidad de objetos detectados*/
bool tecla1=FALSE;
bool tecla2=FALSE;
bool tecla3=FALSE;
uint8_t contadorant=0; /*Variable que guarda el estado anterior del contador*/
	/*Variables tecla1,tecla2,tecla3, son banderas para guardar estados de teclas*/
/*==================[internal data definition]===============================*/
void Delay(void)
{
	uint32_t i;

	for(i=COUNT_DELAY; i!=0; i--)
	{
		   asm  ("nop");
	}
}
void Tecla1(){
	tecla1=!tecla1;
}
void Tecla2(){
	tecla2=!tecla2;
}

void Tecla3(){
	Delay();
	contador=0;
	contadorant=contador;
	tecla3=FALSE;
}
/*Las funciones Tecla1,Tecla2,Tecla3 indican lo que hace el programa de acuerdo a la interrupción que ocurre*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	bool estado=NOBJET; /*Variable que guarda el estado del detector de objetos*/
	bool estadoant=NOBJET;/*Variable que guarda el estado anterior del detector de objetos*/
	SystemClockInit();/*Inicializo clock del CPU*/
	LedsInit(); /*Inicializo leds*/
	SwitchesInit();/*Inicializo las teclas*/
	Tcrt5000Init(GPIO_T_COL0);/*Inicializo el puerto donde está conectado mi dispositivo externo*/
	SwitchActivInt(SWITCH_1,Tecla1);
	SwitchActivInt(SWITCH_2,Tecla2);
	SwitchActivInt(SWITCH_3,Tecla3);
	/*SwitchActivInt ejecuta la interrupción de acuerdo a la tecla que es presionada y el programa ejecuta lo que corresponde a esa interrupción (lo que ocurre es delimitado en la función enviada por puntero) */
	while (1) {
		if (tecla1 == TRUE) { /*Si presiono por primera vez la tecla 1, se prende el contador (entra a este if)*/
			estado = Tcrt5000State();/*Leo y guardo el estado del puerto del Tcrt5000*/
			if ((estado == OBJET) && (estadoant == NOBJET)) { /*Si el estado es 1, detecta un objeto y lo suma al contador de objetos*/
				contador = contador + 1;
				if (tecla2 == FALSE) {/*Si el estado de la tecla 2 es bajo, el estado anterior del contador va a ser igual que el actual, sino, los los led mostrados quedan en el ultimo estado antes de presionarla*/
					contadorant = contador;

				}
			}
			LedsMask(contadorant);/*Muestra el resultado del contador en numero binario*/

			estadoant = estado;/*Le coloca a la variable estado anteror, el valor actual que lee el puerto*/

		} else {/*Si la tecla está en bajo (cero), se apagan todos los leds y el contador vuelve a cero*/
			LedOff(LED_1);
			LedOff(LED_2);
			LedOff(LED_3);
			LedOff(LED_RGB_R);
			LedOff(LED_RGB_B);
			LedOff(LED_RGB_G);
			contador = 0;
			contadorant = contador;

		}

	}









    }

    


/*==================[end of file]============================================*/

