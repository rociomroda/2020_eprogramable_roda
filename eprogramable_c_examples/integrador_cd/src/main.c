/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JM Reta - jmreta@ingenieria.uner.edu.ar
 * Eduardo Filomena
 * Gonzalo Cuenca
 * Juan Ignacio Cerrudo
 * Albano Peñalva
 * Sebastián Mateos
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>


/*==================[macros and definitions]=================================*/
#define OUT 1
#define INCISO_D

/*==================[internal functions declaration]=========================*/


void BinaryToBcd (uint32_t data, uint8_t digits, uint8_t * bcd_number )
{
	uint8_t i;
	uint8_t resto;
	for(i=0; i<digits; i++){
		resto=data % 10;
		data=data/10;
		bcd_number[i]=resto;

	}


}

typedef struct
{
	uint8_t port;				/*!< GPIO port number */
	uint8_t pin;				/*!< GPIO pin number */
	uint8_t dir;				/*!< GPIO direction ‘0’ IN;  ‘1’ OUT */
} gpioConf_t;

gpioConf_t vector[4]= {
		{1,4,1},
		{1,5,1},
		{1,6,1},
		{2,14,1}
};

void Puertos(uint8_t digito,gpioConf_t *vector){
	uint8_t i;
	for(i=0; i<4; i++){
		if(digito & (1<< i)){
			printf("El puerto ");
			printf("%d",vector[i].port);
			printf(".");
			printf("%d",vector[i].pin);
			printf(":está en 1 \r\n");
			}
		else{
			printf("El puerto ");
			printf("%d",vector[i].port);
			printf(".");
			printf("%d",vector[i].pin);
			printf(":está en 0 \r\n");
			}
		}
	}

int main(void)
{
#ifdef INCISO_D
	uint8_t digito=8;
	Puertos(digito,vector);

#else
	uint32_t datos=1452;
	uint8_t dig=4;
	uint8_t bcd[dig];
	BinaryToBcd(datos,dig,bcd);
	uint8_t j;
	for (j=0; j<dig; j++){
		printf("%d Digito en bcd \r\n",bcd[j]);
	}

#endif
	return 0;
}

/*==================[end of file]============================================*/

