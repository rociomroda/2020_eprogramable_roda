# -*- coding: utf-8 -*-

"""

Sistemas de Adquisición y Procesamiento de Señales
Facultad de Ingeniería - UNER

Análisis de armónicos:
    En el siguiente script se ejemplifica el proceso de carga de una señal a 
    partir de una archivo de audio y el cálculo de la distorsión armónica de 
    dicha señal.

Autor: Albano Peñalva
Fecha: Agosto 2020

"""

# Librerías
from scipy import signal
from scipy.io import wavfile
import numpy as np
import matplotlib.pyplot as plt
import funciones_fft

plt.close('all') # cerrar gráficas anteriores

#%% Lectura del archivo de audio 

instrumento = 'guitarra'      
# instrumento = 'piano'         
# instrumento = 'bajo'          
# instrumento = 'trombon'         
# instrumento = 'cello'
filename = instrumento + '.wav'     # nombre de archivo
fs, data = wavfile.read(filename)   # frecuencia de muestreo y datos de la señal

# Definición de parámetro temporales
ts = 1 / fs                     # tiempo de muestreo
N = len(data)                   # número de muestras en el archivo de audio
t = np.linspace(0, N * ts, N)   # vector de tiempo
senial = data[:, 1]             # se extrae un canal de la pista de audio (el audio es estereo)
senial = senial * 3.3 / (2**16 - 1)# se escala la señal a voltios (considerando un CAD de 16bits y Vref 3.3V)

#%% Cálculo de la Transformada de Fourier 

# Se calcula el espectro en potencia
f, senial_fft_pot = funciones_fft. fft_pot(senial, fs)

#%% Detección de armónicos 

# Se detecta la posición de los picos en la FFT
armonicos, _ = signal.find_peaks(senial_fft_pot, 
                                 distance=100*N/fs,
                                 prominence=np.max(senial_fft_pot)/50)

print("La frecuencia fundamental del tono es de {:.2f}Hz".format(f[armonicos[0]]))

#%% Cálculo de Distorsión Armónica 
"""
La distorsión armónica total se define como la relación entre la sumatoria de 
la potencia de los armónicos y la potencia en la frecuencia fundamental, y se 
mide en forma porcentual.

THD = √ ( (P1 + P2 + ... + Pn) / P0 ) . 100 %
"""

# Se calcula el ancho de banda de los picos (ya que el espectro no es una
# delta de dirac perfecta, sino que la energía se distribuye en una banda)
bw =  signal.peak_widths(senial_fft_pot, armonicos, rel_height=0.99)[0].astype(int)
# Se calcula la potencia en la frecuencia fundamental
pot_fund = np.sum(senial_fft_pot[armonicos[0]-bw[0] : armonicos[0]+bw[0]])
# Se calcula la sumatoria de la potencia en los armónicos
pot_sum_arm = 0
for i in range(len(armonicos)-1):
    pot_sum_arm += np.sum(senial_fft_pot[armonicos[i+1]-bw[i+1] : armonicos[i+1]+bw[i+1]])
    
# Se calcula la THD
thd = np.sqrt(pot_sum_arm / pot_fund) * 100

print("La distorsión armónica de la señal es del {:.2f}%".format(thd))

#%% Graficación 

# Se crea una gráfica 
fig3, ax3 = plt.subplots(1, 1, figsize=(20, 10))
fig3.suptitle(instrumento, fontsize=18)

# Se grafica la magnitud del espectro (en potencia) y la ubicación de los armónicos
ax3.plot(f, senial_fft_pot, label='FFT', zorder=1, color='blue')
ax3.plot(f[armonicos[0]-bw[0] : armonicos[0]+bw[0]], 
         senial_fft_pot[armonicos[0]-bw[0] : armonicos[0]+bw[0]], 
          label='Frec. fundamental',  color='red')
ax3.text(f[armonicos[0]], senial_fft_pot[armonicos[0]], 'F', fontsize=15,
         horizontalalignment='center',
         bbox=dict(boxstyle='round', facecolor='white', alpha=0.5))
for i in range(len(armonicos)-1):
    line = ax3.plot(f[armonicos[i+1]-bw[i+1] : armonicos[i+1]+bw[i+1]], 
             senial_fft_pot[armonicos[i+1]-bw[i+1] : armonicos[i+1]+bw[i+1]], 
             color='green')
    ax3.text(f[armonicos[i+1]], senial_fft_pot[armonicos[i+1]], 
                    '{}'.format(i+1), fontsize=15,
                    horizontalalignment='center',
                    bbox=dict(boxstyle='round', facecolor='white', alpha=0.5))
if len(armonicos) > 1:
    line[0].set_label('Armónicos')
ax3.set_xlabel('Frecuencia [Hz]', fontsize=15)
ax3.set_ylabel('Potencia [W]', fontsize=15)
ax3.set_xlim([0, 3000])
ax3.grid()
ax3.legend(fontsize=15)
plt.show()

    
    