/*! @mainpage Ejercicio_Complementario
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 *https://youtu.be/hXC1S3V84P4
 *
 *Object counter starts when switch 1 is true.
 *Leds show the number of count in binary and they are off when the counter is zero or when switch 1 is false.
 *When switch 1 is true and switch 2 too, leds show the last count before push switch 2, but the counter continuous counting objects
 *When switch 3 is true, the counter returns to zero and starts again.
 *When switch 1 is false, all leds are off and counter isn't count.
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	OUT 	    | 	TCOL0	    |
 * |    GND         |   GND         |
 * |    VCC         |   5 V         |
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 13/09/2020 | Document creation		                         |
 * | 09/09/2020 | Functionalities were added                     |
 *
 * @author Roda Rocío Milagros
 *
 */

#ifndef _EJERCICIO_COMPLEMENTARIO_H
#define _EJERCICIO_COMPLEMENTARIO_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _EJERCICIO_COMPLEMENTARIO_H */

